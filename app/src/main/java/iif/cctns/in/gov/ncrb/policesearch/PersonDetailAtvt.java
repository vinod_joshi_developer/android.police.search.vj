package iif.cctns.in.gov.ncrb.policesearch;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.print.PrintManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import iif.cctns.in.gov.ncrb.policesearch.adapter.DetailAdapter;
import iif.cctns.in.gov.ncrb.policesearch.adapter.PersonDetailAdapter;
import iif.cctns.in.gov.ncrb.policesearch.adapter.PersonSearchResultAdapter;
import iif.cctns.in.gov.ncrb.policesearch.pojo.KeyValueListPojo;
import iif.cctns.in.gov.ncrb.policesearch.pojo.LabelPojo;
import iif.cctns.in.gov.ncrb.policesearch.utils.Constant;
import iif.cctns.in.gov.ncrb.policesearch.utils.Singleton;
import iif.cctns.in.gov.ncrb.policesearch.utils.Utility;
import iif.cctns.in.gov.ncrb.policesearch.utils.ViewPrintAdapter;
import iif.cctns.in.gov.ncrb.policesearch.webservice.MasterWebController;
import iif.cctns.in.gov.ncrb.policesearch.webservice.RequestParamsToServer;
import iif.cctns.in.gov.ncrb.policesearch.webservice.ResponseParamsFromServer;

public class PersonDetailAtvt extends AppCompatActivity {
    private Activity _acActivity = PersonDetailAtvt.this;
    RecyclerView rvList;
    private TextView tvHeaderName;


    ProgressDialog mProgressDialog;
    Singleton singleton;
    ImageView iv_accusedimage;
    ResponseParamsFromServer.WSPPersonSearchRS wspPersonSearchRS;
    RequestParamsToServer requestParamsToServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_detail);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Initalize all widgets
        initView();

    }//end onCreate-------------

    private void initView() {

        singleton = Singleton.getInstance();
        requestParamsToServer = new RequestParamsToServer();


        iv_accusedimage = (ImageView) findViewById(R.id.iv_accusedimage);

        tvHeaderName = findViewById(R.id.tvHeaderName);
        tvHeaderName.setText("Person Details");


        findViewById(R.id.btn_fir_person).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.doStartActivityWithoutFinish(PersonDetailAtvt.this, FIROverView.class, "right");
            }
        });

        findViewById(R.id.btn_view_more_person).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.doStartActivityWithoutFinish(PersonDetailAtvt.this, PersonMoreDetailAtvt.class, "right");
            }
        });

        findViewById(R.id.btn_save_pdf_person).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PersonDetailAtvt.this.printPDF(view);
            }
        });


        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        mProgressDialog = new ProgressDialog(PersonDetailAtvt.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");

        try {
            setPersonResult();
        } catch (Exception e) {
            e.printStackTrace();
            Utility.showMessage(Constant.SERVER_NO_DATA, PersonDetailAtvt.this);

        }


    }//end initView------------


    @Override
    public void onStart() {
        super.onStart();



    }// on Start

    private void setPersonResult() throws Exception {

        this.wspPersonSearchRS = singleton.responseParamsFromServer.personSearchList.get(singleton.personSearchRowNo);

        // retain this value for more detail activity page
        singleton.wspPersonSearchRS = this.wspPersonSearchRS;

        // make sure you have image id , else image not load
        if(!this.wspPersonSearchRS.accused_srno.equals("")) LoadAccusedImage();


        List<KeyValueListPojo> keyValueListPojos = new ArrayList<KeyValueListPojo>();
        Gson gson = new Gson(); String json = gson.toJson(wspPersonSearchRS);
        keyValueListPojos = Utility.ConvertObjectToArray(json, LabelPojo.PersonLabel);

        rvList = findViewById(R.id.rvList);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(PersonDetailAtvt.this, LinearLayoutManager.VERTICAL, false);
        rvList.setLayoutManager(mLayoutManager);
        rvList.setItemAnimator(new DefaultItemAnimator());
        //rvList.setAdapter(new PersonSearchResultAdapter(PersonSearchResultActivity.this));
        rvList.setAdapter(new PersonDetailAdapter(PersonDetailAtvt.this, keyValueListPojos));


    }// end setPersonResult


    public void printPDF(View view) {

        PrintManager printManager = (PrintManager) getSystemService(PRINT_SERVICE);

        printManager.print("print_any_view_job_name", new ViewPrintAdapter(this, findViewById(R.id.llmid)), null);
    }


    public void LoadAccusedImage() {

        //
        try {

            mProgressDialog.show();
            MasterWebController masterWebController = new MasterWebController();
            this.requestParamsToServer.m_service = Constant.mGetImagesBlob;
            this.requestParamsToServer.mperson_reg_num = "27541004170000092";
            //this.wspPersonSearchRS.accused_srno //todo change this to real
            masterWebController.GetAccusedImage(this.requestParamsToServer);
            recursionMasterCheckWSPAccusedImageCase();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /**
     * kindly be careful recursion may be infinite if not handled carefully
     */
    private void recursionMasterCheckWSPAccusedImageCase() {

        try {

            if(singleton.resonsemGetImagesBlob == null) {
                mProgressDialog.dismiss();
                Utility.showMessage("Server Error", PersonDetailAtvt.this);
                return;
            }

            if (!singleton.resonsemGetImagesBlob.STATUS_CODE.equals(Constant.recursion_code)) {

                mProgressDialog.dismiss();

                if (singleton.resonsemGetImagesBlob.STATUS_CODE.equals(Constant.SUCCESS_200)) {
                    setImagePerson(singleton.resonsemGetImagesBlob.BASE64IMAGE);
                } else {
                    Utility.showMessage(Constant.SERVER_NO_DATA, PersonDetailAtvt.this);
                }



            } else {

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        recursionMasterCheckWSPAccusedImageCase();
                    }
                }, 2000); // After 5 seconds
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public void setImagePerson(String base64) {

        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);

        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        System.out.println("decoded byte="+ decodedByte );

        this.iv_accusedimage.setImageBitmap(decodedByte);
    }

    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Utility.doAnim(_acActivity, "left");
    }
}//end main class--------------------
