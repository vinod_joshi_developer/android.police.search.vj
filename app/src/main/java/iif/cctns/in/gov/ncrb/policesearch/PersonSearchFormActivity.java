package iif.cctns.in.gov.ncrb.policesearch;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import iif.cctns.in.gov.ncrb.policesearch.pojo.KeyValueListPojo;
import iif.cctns.in.gov.ncrb.policesearch.pojo.KeyValueSpinnerPojo;
import iif.cctns.in.gov.ncrb.policesearch.pojo.KeyValueSpinnerPojo;
import iif.cctns.in.gov.ncrb.policesearch.utils.Constant;
import iif.cctns.in.gov.ncrb.policesearch.utils.Singleton;
import iif.cctns.in.gov.ncrb.policesearch.utils.Utility;
import iif.cctns.in.gov.ncrb.policesearch.webservice.MasterWebController;
import iif.cctns.in.gov.ncrb.policesearch.webservice.RequestParamsToServer;
import iif.cctns.in.gov.ncrb.policesearch.webservice.ResponseParamsFromServer;

public class PersonSearchFormActivity extends AppCompatActivity {
    private Activity _acActivity = PersonSearchFormActivity.this;

    Spinner sp_gender, sp_relation_Type, sp_religion, sp_district, sp_police_station;
    String GenCode, RelgnCode, RelTypCode;// gender, religion, relation
    String Dist_Code, PS_Code; // district  , police station

    Singleton singleton;
    ProgressDialog mProgressDialog;

    CheckBox chkArr;
    CheckBox chkCon;
    CheckBox chkPO;
    CheckBox chkCS;
    CheckBox chkHO;
    CheckBox chkNA;

    EditText edt_person_first_name, edt_person_middle_name, edt_person_last_name, edt_person_alias, edt_Relative_Name;
    EditText edt_person_age_From, edt_person_age_To;
    EditText edt_person_height_From_Ft, edt_person_height_From_Inch, edt_person_height_To_Ft, edt_person_height_To_Inch;

    RequestParamsToServer requestParamsToServer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_person);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        //Initalize all widgets
        initView();



    }//end onCreate-------------

    private void initView() {

        singleton = Singleton.getInstance();

        requestParamsToServer = new RequestParamsToServer();

        sp_religion = (Spinner) findViewById(R.id.sp_religion);
        sp_gender = (Spinner) findViewById(R.id.sp_gender);
        sp_relation_Type = (Spinner) findViewById(R.id.sp_relation_Type);
        sp_district = (Spinner) findViewById(R.id.sp_district);
        sp_police_station = (Spinner) findViewById(R.id.sp_police_station);


        chkArr = (CheckBox) findViewById(R.id.chkArrested);
        chkCon = (CheckBox) findViewById(R.id.chkConvicted);
        chkPO = (CheckBox) findViewById(R.id.chkPO);
        chkCS = (CheckBox) findViewById(R.id.chkCS);
        chkHO = (CheckBox) findViewById(R.id.chkHO);
        chkNA = (CheckBox) findViewById(R.id.chkNA);


        edt_person_first_name = (EditText) findViewById(R.id.edt_person_first_name);
        edt_person_middle_name = (EditText) findViewById(R.id.edt_person_middle_name);
        edt_person_last_name = (EditText) findViewById(R.id.edt_person_last_name);
        edt_person_alias = (EditText) findViewById(R.id.edt_person_alias);

        edt_person_age_From = (EditText) findViewById(R.id.edt_person_age_From);
        edt_person_age_To = (EditText) findViewById(R.id.edt_person_age_To);

        edt_person_height_From_Ft = (EditText) findViewById(R.id.edt_person_height_From_Ft);
        edt_person_height_From_Inch = (EditText) findViewById(R.id.edt_person_height_From_Inch);
        edt_person_height_To_Ft = (EditText) findViewById(R.id.edt_person_height_To_Ft);
        edt_person_height_To_Inch = (EditText) findViewById(R.id.edt_person_height_To_Inch);

        edt_Relative_Name = (EditText) findViewById(R.id.edt_Relative_Name);

        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        findViewById(R.id.tvSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ValidateInput();


            }
        });

        // reset pagination counter
        singleton.pagination_counter=0;


        mProgressDialog = new ProgressDialog(PersonSearchFormActivity.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");


        // load not reload the content
        setGenderData();

        try {

            mProgressDialog.show();
            MasterWebController masterWebController = new MasterWebController();
            RequestParamsToServer requestParamsToServer = new RequestParamsToServer();
            requestParamsToServer.m_service = Constant.mReligionConnect;
            masterWebController.GetReligionSpinner(requestParamsToServer);
            recursionMasterCheckReligion();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }//end initView------------

    @Override
    public void onStart() {
        super.onStart();


    }


    public void ValidateInput() {

        try {
            if (chkArr.isChecked() || chkCon.isChecked() || chkPO.isChecked() || chkCS.isChecked() || chkHO.isChecked() || chkNA.isChecked()) {
                StoreServerInputParams();
            } else {
                Utility.showMessage("Please select any of the search criteria", PersonSearchFormActivity.this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }// end validate


    public void StoreServerInputParams() {


        String height_From_Ft = edt_person_height_From_Ft.getText().equals("")
                ? edt_person_height_From_Ft.getText().toString():"0";


        String height_From_Inch = edt_person_height_From_Inch.getText().equals("")
                ? edt_person_height_From_Inch.getText().toString():"0";

        Utility.printv("height_From_Ft "+height_From_Ft+"height_From_Inch "+height_From_Inch);

        int Height_From_Ft=Integer.parseInt(height_From_Ft);
        int Height_From_Inch=Integer.parseInt(height_From_Inch);
        double Height_From_cm=2.54*((Height_From_Ft*12)+Height_From_Inch);

        String height_To_Ft = edt_person_height_To_Ft.getText().equals("")
                ? edt_person_height_To_Ft.getText().toString():"0";
        String height_To_Inch = edt_person_height_To_Inch.getText().equals("")
                ? edt_person_height_To_Inch.getText().toString():"0";

        Utility.printv("height_To_Ft "+height_To_Ft+"height_To_Inch "+height_To_Inch);

        int Height_To_Ft=Integer.parseInt(height_To_Ft);
        int Height_To_Inch=Integer.parseInt(height_To_Inch);
        double Height_To_cm=2.54*((Height_To_Ft*12)+Height_To_Inch);

        // @very important
        RequestParamsToServer personSearchFormActivityInput = new RequestParamsToServer();
        personSearchFormActivityInput.m_service = Constant.mPersonSearch; // mandatory
        personSearchFormActivityInput.pagination_counter = ""+singleton.pagination_counter; //mandatory

        personSearchFormActivityInput.P_STATE_CD = Constant.STATE_CD;
        personSearchFormActivityInput.P_DISTRICT_CD =  this.Dist_Code;
        personSearchFormActivityInput.P_PS_CD =  this.PS_Code;
        personSearchFormActivityInput.MATCH_CRITERIA_ARR =  (chkArr.isChecked()) ? "true" : "";
        personSearchFormActivityInput.MATCH_CRITERIA_CON =  (chkCon.isChecked()) ? "true" : "";
        personSearchFormActivityInput.MATCH_CRITERIA_PO =  (chkPO.isChecked()) ? "true" : "";
        personSearchFormActivityInput.MATCH_CRITERIA_CS =  (chkCS.isChecked()) ? "true" : "";
        personSearchFormActivityInput.MATCH_CRITERIA_HO =  (chkHO.isChecked()) ? "true" : "";
        personSearchFormActivityInput.MATCH_CRITERIA_NOTARR =  (chkNA.isChecked()) ? "true" : "";
        personSearchFormActivityInput.P_FName = edt_person_first_name.getText().toString();
        personSearchFormActivityInput.P_MName = edt_person_middle_name.getText().toString();
        personSearchFormActivityInput.P_LName = edt_person_last_name.getText().toString();
        personSearchFormActivityInput.P_Alias = edt_person_alias.getText().toString();
        personSearchFormActivityInput.P_GCODE =  this.GenCode;
        personSearchFormActivityInput.P_RELTYPECODE =  this.RelTypCode;
        personSearchFormActivityInput.P_RelativeName =  edt_Relative_Name.getText().toString();
        personSearchFormActivityInput.P_AgeFrom =  edt_person_age_From.getText().toString();
        personSearchFormActivityInput.P_AgeTo =  edt_person_age_To.getText().toString();
        personSearchFormActivityInput.P_HEIGHTFROM =  ""+Height_From_cm;
        personSearchFormActivityInput.P_HEIGHTTO =  ""+Height_To_cm;
        personSearchFormActivityInput.P_RELGNCODE =  this.RelgnCode;

        // access to any class in whole project
        singleton.personSearchFormActivityInput = personSearchFormActivityInput;


        List<KeyValueListPojo> keyValueListPojos = new ArrayList<>();
        Gson gson = new Gson(); String json = gson.toJson(personSearchFormActivityInput);
        keyValueListPojos = Utility.ConvertObjectToArray(json, this.getClass().getSimpleName());


        // lets go to next
        Utility.doStartActivityWithoutFinish(_acActivity, PersonSearchResultActivity.class, "right");

    }// end validate



    private void setGenderData() {


        ArrayList<KeyValueSpinnerPojo> Genderlist = new ArrayList<>();


        Genderlist.add(new KeyValueSpinnerPojo("", "--Select Gender--"));
        Genderlist.add(new KeyValueSpinnerPojo("3", "Male"));
        Genderlist.add(new KeyValueSpinnerPojo("2", "Female"));
        Genderlist.add(new KeyValueSpinnerPojo("1", "Transgender"));
        Genderlist.add(new KeyValueSpinnerPojo("4", "Unknown"));


        //fill data in spinner
        ArrayAdapter<KeyValueSpinnerPojo> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, Genderlist);
        sp_gender.setAdapter(adapter);
        sp_gender.setSelection(0);//Optional to set the selected item.

        sp_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                KeyValueSpinnerPojo Gender = (KeyValueSpinnerPojo) parent.getSelectedItem();
                //Utility.showMessage("Gender ID: " + Gender.getId() + ",  Gender Name : " + Gender.getName(), PersonSearchFormActivity.this);
                PersonSearchFormActivity.this.GenCode = Gender.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    /**
     * kindly be careful recursion may be infinite if not handled carefully
     */
    private void recursionMasterCheckReligion() {


        if (!singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.recursion_code)) {

            if (singleton.responseParamsFromServer == null) {
                Utility.showMessage("Please try later. Server not responding", PersonSearchFormActivity.this);
                return;
            }

            mProgressDialog.dismiss();

            if (singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.SUCCESS_200)) {
                setReligionData();
            } else {
                Utility.showMessage(Constant.SERVER_NO_DATA, PersonSearchFormActivity.this);
            }


        } else {

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    recursionMasterCheckReligion();
                }
            }, 2000); // After 5 seconds
        }
    }

    /**
     * kindly be careful recursion may be infinite if not handled carefully
     */
    private void recursionMasterCheckRelation() {

        if (!singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.recursion_code)) {

            if (singleton.responseParamsFromServer == null) {
                Utility.showMessage("Please try later. Server not responding", PersonSearchFormActivity.this);
                return;
            }

            mProgressDialog.dismiss();

            if (singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.SUCCESS_200)) {
                setRelationData();
            } else {
                Utility.showMessage(Constant.SERVER_NO_DATA, PersonSearchFormActivity.this);
            }


        } else {

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    recursionMasterCheckRelation();
                }
            }, 2000); // After 5 seconds
        }
    }


    /**
     * kindly be careful recursion may be infinite if not handled carefully
     */
    private void recursionMasterCheckDistrict() {

        if (!singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.recursion_code)) {

            if (singleton.responseParamsFromServer == null) {
                Utility.showMessage("Please try later. Server not responding", PersonSearchFormActivity.this);
                return;
            }

            mProgressDialog.dismiss();

            if (singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.SUCCESS_200)) {
                setDistrictData();
            } else {
                Utility.showMessage(Constant.SERVER_NO_DATA, PersonSearchFormActivity.this);
            }



        } else {

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    recursionMasterCheckDistrict();
                }
            }, 2000); // After 5 seconds
        }
    }


    /**
     * kindly be careful recursion may be infinite if not handled carefully
     */
    private void recursionMasterCheckPoliceStation() {

        if (!singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.recursion_code)) {


            if (singleton.responseParamsFromServer == null) {
                Utility.showMessage("Please try later. Server not responding", PersonSearchFormActivity.this);
                return;
            }

            mProgressDialog.dismiss();

            if (singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.SUCCESS_200)) {
                setPoliceStationData();
            } else {
                Utility.showMessage(Constant.SERVER_NO_DATA, PersonSearchFormActivity.this);
            }



        } else {

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    recursionMasterCheckPoliceStation();
                }
            }, 2000); // After 5 seconds
        }
    }


    private void setReligionData() {


        ArrayList<KeyValueSpinnerPojo> religionlist = new ArrayList<>();


        religionlist.add(new KeyValueSpinnerPojo("", "--Select Religion--"));

        for (ResponseParamsFromServer.WSPReligionRS rs : singleton.responseParamsFromServer.ReligionList) {
            religionlist.add(new KeyValueSpinnerPojo(rs.ReligionCd, rs.ReligionName));
        }


        //fill data in spinner
        ArrayAdapter<KeyValueSpinnerPojo> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, religionlist);
        sp_religion.setAdapter(adapter);
        sp_religion.setSelection(0);//Optional to set the selected item.

        sp_religion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                KeyValueSpinnerPojo religion = (KeyValueSpinnerPojo) parent.getSelectedItem();
                //Utility.showMessage("religion ID: " + religion.getId() + ",  religion Name : " + religion.getName(), PersonSearchFormActivity.this);
                PersonSearchFormActivity.this.RelgnCode = religion.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // get all relation after getting religions
        try {

            mProgressDialog.show();
            MasterWebController masterWebController = new MasterWebController();
            RequestParamsToServer requestParamsToServer = new RequestParamsToServer();
            requestParamsToServer.m_service = Constant.mRelationTypeConnect;
            masterWebController.GetRelationSpinner(requestParamsToServer);
            recursionMasterCheckRelation();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void setRelationData() {


        ArrayList<KeyValueSpinnerPojo> relationlist = new ArrayList<>();


        relationlist.add(new KeyValueSpinnerPojo("", "--Select Relation Type--"));

        for (ResponseParamsFromServer.WSPRelationTypeRS rs : singleton.responseParamsFromServer.RelationTypeList) {
            relationlist.add(new KeyValueSpinnerPojo(rs.RelationTypeCd, rs.RelationType));
        }


        //fill data in spinner
        ArrayAdapter<KeyValueSpinnerPojo> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, relationlist);
        sp_relation_Type.setAdapter(adapter);
        sp_relation_Type.setSelection(0);//Optional to set the selected item.

        sp_relation_Type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                KeyValueSpinnerPojo relation = (KeyValueSpinnerPojo) parent.getSelectedItem();
                //Utility.showMessage("relation ID: " + relation.getId() + ",  relation Name : " + relation.getName(), PersonSearchFormActivity.this);
                PersonSearchFormActivity.this.RelTypCode = relation.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        // get all relation after getting religions
        try {

            mProgressDialog.show();
            MasterWebController masterWebController = new MasterWebController();
            requestParamsToServer.m_service = Constant.mDistrictConnect;
            requestParamsToServer.FxdState_Cd = Constant.STATE_CD;
            masterWebController.GetDistrictSpinner(requestParamsToServer);
            recursionMasterCheckDistrict();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void setDistrictData() {


        ArrayList<KeyValueSpinnerPojo> districtlist = new ArrayList<>();


        districtlist.add(new KeyValueSpinnerPojo("", "--Select District--"));

        for (ResponseParamsFromServer.WSPDistrict rs : singleton.responseParamsFromServer.DistrictList) {
            districtlist.add(new KeyValueSpinnerPojo(rs.DistrictCd, rs.DistrictName));
        }


        //fill data in spinner
        ArrayAdapter<KeyValueSpinnerPojo> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, districtlist);
        sp_district.setAdapter(adapter);
        sp_district.setSelection(0);//Optional to set the selected item.

        sp_district.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                KeyValueSpinnerPojo district = (KeyValueSpinnerPojo) parent.getSelectedItem();
                //Utility.showMessage("district ID: " + district.getId() + ",  district Name : " + district.getName(), PersonSearchFormActivity.this);
                PersonSearchFormActivity.this.Dist_Code = district.getId();

                //
                try {

                    mProgressDialog.show();
                    MasterWebController masterWebController = new MasterWebController();
                    PersonSearchFormActivity.this.requestParamsToServer.m_service = Constant.mPoliceStationConnect;
                    PersonSearchFormActivity.this.requestParamsToServer.District_Cd = district.getId();// todo change default
                    masterWebController.GetPoliceStationSpinner(PersonSearchFormActivity.this.requestParamsToServer);
                    recursionMasterCheckPoliceStation();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }

    private void setPoliceStationData() {


        ArrayList<KeyValueSpinnerPojo> policestationlist = new ArrayList<>();


        policestationlist.add(new KeyValueSpinnerPojo("", "--Select Police Station--"));

        for (ResponseParamsFromServer.WSPPoliceStation rs : singleton.responseParamsFromServer.PoliceStationList) {
            policestationlist.add(new KeyValueSpinnerPojo(rs.PoliceStationCd, rs.PoliceStationName));
        }


        //fill data in spinner
        ArrayAdapter<KeyValueSpinnerPojo> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, policestationlist);
        sp_police_station.setAdapter(adapter);
        sp_police_station.setSelection(0);//Optional to set the selected item.

        sp_police_station.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                KeyValueSpinnerPojo policestation = (KeyValueSpinnerPojo) parent.getSelectedItem();
                //Utility.showMessage("policestation ID: " + policestation.getId() + ",  policestation Name : " + policestation.getName(), PersonSearchFormActivity.this);
                PersonSearchFormActivity.this.PS_Code = policestation.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Utility.doAnim(_acActivity, "left");
    }


}//end main class--------------------
