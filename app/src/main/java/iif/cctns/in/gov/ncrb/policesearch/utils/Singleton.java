package iif.cctns.in.gov.ncrb.policesearch.utils;


import iif.cctns.in.gov.ncrb.policesearch.pojo.KeyValueListPojo;
import iif.cctns.in.gov.ncrb.policesearch.webservice.RequestParamsToServer;
import iif.cctns.in.gov.ncrb.policesearch.webservice.ResponseParamsFromServer;

/**
 * Created by Lenovo on 13-04-2016.
 */
public class Singleton {

    private static Singleton singleton = new Singleton( );

    public String username ;
    public ResponseParamsFromServer responseParamsFromServer;
    public RequestParamsToServer personSearchFormActivityInput;
    public RequestParamsToServer propertySearchFormActivityInput;
    public int personSearchRowNo;
    public int propertySearchRowNo;
    public ResponseParamsFromServer.WSPPersonSearchRS wspPersonSearchRS;


    public ResponseParamsFromServer resonsemGetImagesBlob;


    //view fir Params
//    public String FIR_firregnum;
//    public String FIR_district_cd;
//    public String FIR_ps_cd;
//    public String FIR_year;

    public String FIR_firregnum="0001";
    public String FIR_district_cd="13240";
    public String FIR_ps_cd="13240012";
    public String FIR_year="2016";


    public int pagination_counter=0;



    /**
     *
     * A private Constructor prevents any other
     * class from instantiating.
     *
     */
    private Singleton(){ }

    /* Static 'instance' method */
    public static Singleton getInstance( ) {
        return singleton;
    }

    /* Other methods protected by singleton-ness */
    protected static void demoMethod( ) {
        System.out.println("demoMethod for singleton");
    }

}// end main singleton
