package iif.cctns.in.gov.ncrb.policesearch;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.print.PrintManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.security.spec.ECField;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import iif.cctns.in.gov.ncrb.policesearch.adapter.PersonDetailAdapter;
import iif.cctns.in.gov.ncrb.policesearch.adapter.PersonMoreDetailAdapter;
import iif.cctns.in.gov.ncrb.policesearch.adapter.PersonSearchResultAdapter;
import iif.cctns.in.gov.ncrb.policesearch.pojo.KeyValueListPojo;
import iif.cctns.in.gov.ncrb.policesearch.pojo.LabelPojo;
import iif.cctns.in.gov.ncrb.policesearch.utils.Constant;
import iif.cctns.in.gov.ncrb.policesearch.utils.Singleton;
import iif.cctns.in.gov.ncrb.policesearch.utils.Utility;
import iif.cctns.in.gov.ncrb.policesearch.utils.ViewPrintAdapter;
import iif.cctns.in.gov.ncrb.policesearch.webservice.MasterWebController;
import iif.cctns.in.gov.ncrb.policesearch.webservice.RequestParamsToServer;
import iif.cctns.in.gov.ncrb.policesearch.webservice.ResponseParamsFromServer;

public class PersonMoreDetailAtvt extends AppCompatActivity {
    private Activity _acActivity = PersonMoreDetailAtvt.this;
    RecyclerView rvList;
    private TextView tvHeaderName;


    ProgressDialog mProgressDialog;
    Singleton singleton;
    RequestParamsToServer requestParamsToServer;
    ResponseParamsFromServer.WSPArrestCase wspArrestCase;
    ResponseParamsFromServer.WSPConvictCase wspConvictCase;
    ResponseParamsFromServer.WSPOffenderCase wspOffenderCase;

    List<KeyValueListPojo> keyValueListPojosChargesheet;
    List<KeyValueListPojo> keyValueListPojosOffender;
    List<KeyValueListPojo> keyValueListPojosConvict;
    List<KeyValueListPojo> keyValueListPojosArrest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_more_detail);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Initalize all widgets
        initView();

    }//end onCreate-------------

    private void initView() {

        singleton = Singleton.getInstance();

        tvHeaderName = findViewById(R.id.tvHeaderName);
        tvHeaderName.setText("Person Details");

        findViewById(R.id.btn_save_pdf_person).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PersonMoreDetailAtvt.this.printPDF(view);
            }
        });


        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }//end initView------------



    @Override
    public void onStart() {
        super.onStart();

        keyValueListPojosArrest = new ArrayList<KeyValueListPojo>();
        keyValueListPojosChargesheet = new ArrayList<KeyValueListPojo>();
        keyValueListPojosOffender = new ArrayList<KeyValueListPojo>();
        keyValueListPojosConvict = new ArrayList<KeyValueListPojo>();

        requestParamsToServer = new RequestParamsToServer();

        mProgressDialog = new ProgressDialog(PersonMoreDetailAtvt.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");


        //
        try {

            mProgressDialog.show();
            MasterWebController masterWebController = new MasterWebController();
            PersonMoreDetailAtvt.this.requestParamsToServer.m_service = Constant.mPersonMoreDetails;

            PersonMoreDetailAtvt.this.requestParamsToServer.ACCUSED_SRNO = singleton.wspPersonSearchRS.accused_srno;
            PersonMoreDetailAtvt.this.requestParamsToServer.FIR_REG_NUM = singleton.wspPersonSearchRS.REGISTRATION_NUM;

            PersonMoreDetailAtvt.this.requestParamsToServer.ACCUSED_SRNO = "27546053170000104";
            PersonMoreDetailAtvt.this.requestParamsToServer.FIR_REG_NUM = "27546053170046";


            masterWebController.GetMoreDetailPerson(PersonMoreDetailAtvt.this.requestParamsToServer);
            recursionMasterCheckWSPArrestCase();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * kindly be careful recursion may be infinite if not handled carefully
     */
    private void recursionMasterCheckWSPArrestCase() {

       try {

           if(singleton.responseParamsFromServer == null) {
               mProgressDialog.dismiss();
               Utility.showMessage(Constant.SERVER_NO_DATA, PersonMoreDetailAtvt.this);
               return;
           }


           if (!singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.recursion_code)) {

               mProgressDialog.dismiss();

               if (singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.SUCCESS_200)) {
                   setWSPArrestCaseResult();
               } else {
                   //Utility.showMessage(Constant.SERVER_NO_DATA, PersonMoreDetailAtvt.this);
                   LoadConvictData();
               }

           } else {

               final Handler handler = new Handler();
               handler.postDelayed(new Runnable() {
                   @Override
                   public void run() {
                       recursionMasterCheckWSPArrestCase();
                   }
               }, 2000); // After 5 seconds
           }
       }catch (Exception e){
           e.printStackTrace();
       }


    }


    private void setWSPArrestCaseResult() throws Exception {

        //todo default value =0 bez we don't have more than 1 in  array
        this.wspArrestCase = singleton.responseParamsFromServer.PersonMoreDetailsList.get(0);

        Gson gson = new Gson(); String json = gson.toJson(this.wspArrestCase);

        this.keyValueListPojosArrest = Utility.ConvertObjectToArray(json, LabelPojo.MoreDetailPersonLabel);

        LoadConvictData();

    }// end setWSPArrestCaseResult


    public void LoadConvictData(){

        //
        try {

            mProgressDialog.show();
            MasterWebController masterWebController = new MasterWebController();
            PersonMoreDetailAtvt.this.requestParamsToServer.m_service = Constant.mPersonMoreDetailsConvict;
            PersonMoreDetailAtvt.this.requestParamsToServer.ACCUSED_SRNO = singleton.wspPersonSearchRS.accused_srno;
            PersonMoreDetailAtvt.this.requestParamsToServer.FIR_REG_NUM = singleton.wspPersonSearchRS.REGISTRATION_NUM;
            masterWebController.GetMoreDetailPersonConvict(PersonMoreDetailAtvt.this.requestParamsToServer);
            recursionMasterCheckWSPConvictCase();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * kindly be careful recursion may be infinite if not handled carefully
     */
    private void recursionMasterCheckWSPConvictCase() {

        try {


            if(singleton.responseParamsFromServer == null) {
                mProgressDialog.dismiss();
                Utility.showMessage("Server Error", PersonMoreDetailAtvt.this);
                return;
            }

            if (!singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.recursion_code)) {

                mProgressDialog.dismiss();

                if (singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.SUCCESS_200)) {
                    setWSPConvictCaseResult();
                } else {
                    LoadOffender();
                    //Utility.showMessage(Constant.SERVER_NO_DATA, PersonMoreDetailAtvt.this);
                }



            } else {

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        recursionMasterCheckWSPConvictCase();
                    }
                }, 2000); // After 5 seconds
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }


    private void setWSPConvictCaseResult() throws Exception {

        //todo default value =0 bez we don't have more than 1 in  array
        this.wspConvictCase = singleton.responseParamsFromServer.PersonMoreDetailsConvictList.get(0);

        Gson gson = new Gson(); String json = gson.toJson(this.wspConvictCase);

        this.keyValueListPojosConvict = Utility.ConvertObjectToArray(json,LabelPojo.PersonMoreDetailsConvictLabel);

        LoadOffender();

    }// end setWSPConvictCaseResult


    public void LoadOffender() {

        //
        try {

            mProgressDialog.show();
            MasterWebController masterWebController = new MasterWebController();
            PersonMoreDetailAtvt.this.requestParamsToServer.m_service = Constant.mPersonMoreDetailsPHOffender;
            PersonMoreDetailAtvt.this.requestParamsToServer.ACCUSED_SRNO = singleton.wspPersonSearchRS.accused_srno;
            PersonMoreDetailAtvt.this.requestParamsToServer.FIR_REG_NUM = singleton.wspPersonSearchRS.REGISTRATION_NUM;
            masterWebController.GetMoreDetailPersonPHOffender(PersonMoreDetailAtvt.this.requestParamsToServer);
            recursionMasterCheckWSPOffenderCase();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /**
     * kindly be careful recursion may be infinite if not handled carefully
     */
    private void recursionMasterCheckWSPOffenderCase() {

        try {


            if(singleton.responseParamsFromServer == null) {
                mProgressDialog.dismiss();
                Utility.showMessage("Server Error", PersonMoreDetailAtvt.this);
                return;
            }


            if (!singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.recursion_code)) {

                mProgressDialog.dismiss();

                if (singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.SUCCESS_200)) {
                    setWSPOffenderCaseResult();
                } else {
                    Utility.showMessage(Constant.SERVER_NO_DATA, PersonMoreDetailAtvt.this);
                }



            } else {

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        recursionMasterCheckWSPOffenderCase();
                    }
                }, 2000); // After 5 seconds
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        // after finishing all networking , load the list
        PersonMoreDetailAtvt.this.loadDetailList();

    }


    private void setWSPOffenderCaseResult() throws Exception {

        //todo default value =0 bez we don't have more than 1 in  array
        this.wspOffenderCase = singleton.responseParamsFromServer.PersonMoreDetailsPHOffenderList.get(0);

        Gson gson = new Gson(); String json = gson.toJson(this.wspOffenderCase);

        this.keyValueListPojosOffender = Utility.ConvertObjectToArray(json,LabelPojo.PersonMoreDetailsPHOffenderLabel);

        // we have loaded arrest, convict, offender,
        //todo chargesheet pending

    }// end setWSPOffenderCaseResult


    public void loadDetailList() {


        List<KeyValueListPojo> keyValueListPojos = new ArrayList<KeyValueListPojo>();

        if(this.keyValueListPojosArrest!=null)keyValueListPojos.addAll(this.keyValueListPojosArrest);
        if(this.keyValueListPojosConvict!=null)keyValueListPojos.addAll(this.keyValueListPojosConvict);
        if(this.keyValueListPojosChargesheet!=null)keyValueListPojos.addAll(this.keyValueListPojosChargesheet);
        if(this.keyValueListPojosOffender!=null)keyValueListPojos.addAll(this.keyValueListPojosOffender);


        rvList = findViewById(R.id.rvList);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(PersonMoreDetailAtvt.this, LinearLayoutManager.VERTICAL, false);
        rvList.setLayoutManager(mLayoutManager);
        rvList.setItemAnimator(new DefaultItemAnimator());
        rvList.setAdapter(new PersonMoreDetailAdapter(PersonMoreDetailAtvt.this, keyValueListPojos));

    }

    public void printPDF(View view) {

        PrintManager printManager = (PrintManager) getSystemService(PRINT_SERVICE);

        printManager.print("print_any_view_job_name", new ViewPrintAdapter(this, findViewById(R.id.llmid)), null);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Utility.doAnim(_acActivity, "left");
    }
}//end main class--------------------
