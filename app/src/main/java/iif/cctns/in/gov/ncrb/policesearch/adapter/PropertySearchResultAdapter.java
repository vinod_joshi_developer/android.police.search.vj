package iif.cctns.in.gov.ncrb.policesearch.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import iif.cctns.in.gov.ncrb.policesearch.PropertyDetailActivity;
import iif.cctns.in.gov.ncrb.policesearch.R;
import iif.cctns.in.gov.ncrb.policesearch.utils.Singleton;
import iif.cctns.in.gov.ncrb.policesearch.utils.Utility;
import iif.cctns.in.gov.ncrb.policesearch.webservice.ResponseParamsFromServer;


/**
 * Created by Vishnu
 */
public class PropertySearchResultAdapter extends RecyclerView.Adapter<PropertySearchResultAdapter.MyViewHolder> {

    Activity mContext;
    Singleton singleton;
    private List<ResponseParamsFromServer.WSPPropertySearchRS> propertySearchCheck;


    public PropertySearchResultAdapter(Activity activity, List<ResponseParamsFromServer.WSPPropertySearchRS> propertySearchCheck) {
        this.mContext = activity;
        this.propertySearchCheck = propertySearchCheck;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_vehicle_reg_no, txt_vehicle_type, txt_vehicle_Model;

        private LinearLayout llMain;

        public MyViewHolder(View view) {
            super(view);
            llMain = view.findViewById(R.id.llMain);
            txt_vehicle_reg_no = view.findViewById(R.id.txt_vehicle_reg_no);
            txt_vehicle_type = view.findViewById(R.id.txt_vehicle_type);
            txt_vehicle_Model = view.findViewById(R.id.txt_vehicle_Model);
        }

    }// end MyViewHolder class...........


    // add single item view
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_search_property_result, parent, false);

        return new MyViewHolder(itemView);
    }

    // Used to set Tire
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        ResponseParamsFromServer.WSPPropertySearchRS wspPropertySearchRS = propertySearchCheck.get(position);
        holder.txt_vehicle_reg_no.setText(wspPropertySearchRS.registrationNo);
        holder.txt_vehicle_type.setText(wspPropertySearchRS.mvType);
        holder.txt_vehicle_Model.setText(wspPropertySearchRS.mvModel);


        holder.llMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                singleton = Singleton.getInstance();
                singleton.propertySearchRowNo = position;
                Utility.doStartActivityWithoutFinish(mContext, PropertyDetailActivity.class, "right");
            }
        });


    }


    @Override
    public int getItemCount() {
        return this.propertySearchCheck.size();
    }

}// end main class............................