package iif.cctns.in.gov.ncrb.policesearch.webservice;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;


public interface ApiCaller {


    /**
     * API caller for JAVA stack
     **/

//    @Headers("Connection:close")
//    @POST("mGetCitizenLoginDetailsConnect.json")
//    Call<ResponseParamsFromServer> mGetCitizenLoginDetailsConnect(@Body RequestParamsToServer jsonPostParams);
//
//
//    @Headers("Connection:close")
//    @POST("mVehicleTypeConnect.json")
//    Call<ResponseParamsFromServer> mVehicleTypeConnect(@Body RequestParamsToServer jsonPostParams);
//
//
//    @Headers("Connection:close")
//    @POST("mPropertySearch.json")
//    Call<ResponseParamsFromServer> mPropertySearch(@Body RequestParamsToServer jsonPostParams);
//
//
//    @Headers("Connection:close")
//    @POST("mManufacturerConnect.json")
//    Call<ResponseParamsFromServer> mManufacturerConnect(@Body RequestParamsToServer jsonPostParams);
//
//
//    @Headers("Connection:close")
//    @POST("mRelationTypeConnect.json")
//    Call<ResponseParamsFromServer> mRelationTypeConnect(@Body RequestParamsToServer jsonPostParams);
//
//    @Headers("Connection:close")
//    @POST("mReligionConnect.json")
//    Call<ResponseParamsFromServer> mReligionConnect(@Body RequestParamsToServer jsonPostParams);
//
//
//    @Headers("Connection:close")
//    @POST("mMVColorConnect.json")
//    Call<ResponseParamsFromServer> mMVColorConnect(@Body RequestParamsToServer jsonPostParams);
//
//
//    @Headers("Connection:close")
//    @POST("mMVModelConnect.json")
//    Call<ResponseParamsFromServer> mMVModelConnect(@Body RequestParamsToServer jsonPostParams);
//
//
//    @Headers("Connection:close")
//    @POST("mPersonSearch.json")
//    Call<ResponseParamsFromServer> mPersonSearch(@Body RequestParamsToServer jsonPostParams);
//
//
//    @Headers("Connection:close")
//    @POST("mDistrictConnect.json")
//    Call<ResponseParamsFromServer> mDistrictConnect(@Body RequestParamsToServer jsonPostParams);
//
//
//    @Headers("Connection:close")
//    @POST("mPoliceStationConnect.json")
//    Call<ResponseParamsFromServer> mPoliceStationConnect(@Body RequestParamsToServer jsonPostParams);
//
//    @Headers("Connection:close")
//    @POST("MainEntryPoint")
//    Call<ResponseParamsFromServer> mGetFIRStatus(@Body RequestParamsToServer jsonPostParams);
//
//
//    @Headers("Connection:close")
//    @POST("mFIRReportConnect.json")
//    Call<ResponseParamsFromServer> mFIRReportConnect(@Body RequestParamsToServer jsonPostParams);
//
//
//    @Headers("Connection:close")
//    @POST("mPersonMoreDetails.json")
//    Call<ResponseParamsFromServer> mPersonMoreDetails(@Body RequestParamsToServer jsonPostParams);
//
//
//    @Headers("Connection:close")
//    @POST("mPersonMoreDetailsChargesheet.json")
//    Call<ResponseParamsFromServer> mPersonMoreDetailsChargesheet(@Body RequestParamsToServer jsonPostParams);
//
//    @Headers("Connection:close")
//    @POST("mPersonMoreDetailsConvict.json")
//    Call<ResponseParamsFromServer> mPersonMoreDetailsConvict(@Body RequestParamsToServer jsonPostParams);
//
//    @Headers("Connection:close")
//    @POST("mPersonMoreDetailsPHOffender.json")
//    Call<ResponseParamsFromServer> mPersonMoreDetailsPHOffender(@Body RequestParamsToServer jsonPostParams);
//
//    @Headers("Connection:close")
//    @POST("mGetImagesBlob.json")
//    Call<ResponseParamsFromServer> mGetImagesBlob(@Body RequestParamsToServer jsonPostParams);


    //// ---------------------- real -------------

    @Headers("Connection:close")
    @POST("MainEntryPoint")
    Call<ResponseParamsFromServer> mGetCitizenLoginDetailsConnect(@Body RequestParamsToServer jsonPostParams);


    @Headers("Connection:close")
    @POST("MainEntryPoint")
    Call<ResponseParamsFromServer> mVehicleTypeConnect(@Body RequestParamsToServer jsonPostParams);


    @Headers("Connection:close")
    @POST("MainEntryPoint")
    Call<ResponseParamsFromServer> mPropertySearch(@Body RequestParamsToServer jsonPostParams);


    @Headers("Connection:close")
    @POST("MainEntryPoint")
    Call<ResponseParamsFromServer> mManufacturerConnect(@Body RequestParamsToServer jsonPostParams);


    @Headers("Connection:close")
    @POST("MainEntryPoint")
    Call<ResponseParamsFromServer> mRelationTypeConnect(@Body RequestParamsToServer jsonPostParams);

    @Headers("Connection:close")
    @POST("MainEntryPoint")
    Call<ResponseParamsFromServer> mReligionConnect(@Body RequestParamsToServer jsonPostParams);


    @Headers("Connection:close")
    @POST("MainEntryPoint")
    Call<ResponseParamsFromServer> mMVColorConnect(@Body RequestParamsToServer jsonPostParams);


    @Headers("Connection:close")
    @POST("MainEntryPoint")
    Call<ResponseParamsFromServer> mMVModelConnect(@Body RequestParamsToServer jsonPostParams);


    @Headers("Connection:close")
    @POST("MainEntryPoint")
    Call<ResponseParamsFromServer> mPersonSearch(@Body RequestParamsToServer jsonPostParams);


    @Headers("Connection:close")
    @POST("MainEntryPoint")
    Call<ResponseParamsFromServer> mDistrictConnect(@Body RequestParamsToServer jsonPostParams);


    @Headers("Connection:close")
    @POST("MainEntryPoint")
    Call<ResponseParamsFromServer> mPoliceStationConnect(@Body RequestParamsToServer jsonPostParams);


    @Headers("Connection:close")
    @POST("MainEntryPoint")
    Call<ResponseParamsFromServer> mGetFIRStatus(@Body RequestParamsToServer jsonPostParams);


    @Headers("Connection:close")
    @POST("MainEntryPoint")
    Call<ResponseParamsFromServer> mPersonMoreDetails(@Body RequestParamsToServer jsonPostParams);


    @Headers("Connection:close")
    @POST("MainEntryPoint")
    Call<ResponseParamsFromServer> mPersonMoreDetailsChargesheet(@Body RequestParamsToServer jsonPostParams);

    @Headers("Connection:close")
    @POST("MainEntryPoint")
    Call<ResponseParamsFromServer> mPersonMoreDetailsConvict(@Body RequestParamsToServer jsonPostParams);

    @Headers("Connection:close")
    @POST("MainEntryPoint")
    Call<ResponseParamsFromServer> mPersonMoreDetailsPHOffender(@Body RequestParamsToServer jsonPostParams);

    @Headers("Connection:close")
    @POST("MainEntryPoint")
    Call<ResponseParamsFromServer> mGetImagesBlob(@Body RequestParamsToServer jsonPostParams);


}// end api caller
