package iif.cctns.in.gov.ncrb.policesearch.webservice;



import android.app.ProgressDialog;
import android.support.annotation.Keep;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import iif.cctns.in.gov.ncrb.policesearch.HomeActivity;
import iif.cctns.in.gov.ncrb.policesearch.LoginActivity;
import iif.cctns.in.gov.ncrb.policesearch.utils.Constant;
import iif.cctns.in.gov.ncrb.policesearch.utils.Singleton;
import iif.cctns.in.gov.ncrb.policesearch.utils.Utility;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Class to represent a category in app.
 * @author
 */
public class MasterWebController {



    Singleton singleton = Singleton.getInstance();


    private OkHttpClient getClient() {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .build();
        return client;
    }

    public void GetAuthDetailWebService(RequestParamsToServer requestParamsToServer) throws Exception {

        //------------- @important default ----------

        // this proof no server call have been made
        ResponseParamsFromServer responseParamsFromServer = new ResponseParamsFromServer();
        responseParamsFromServer.STATUS_CODE=Constant.recursion_code;

        //by default response from this master
        singleton.responseParamsFromServer = responseParamsFromServer;

        //------------- @end default ----------


        MasterWebController.LoginServerController controller = new MasterWebController.LoginServerController();
        controller.start(requestParamsToServer);

    }


    public class LoginServerController implements Callback<ResponseParamsFromServer> {


        public void start(RequestParamsToServer requestParamsToServer) {


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiCaller gerritAPI = retrofit.create(ApiCaller.class);

            Call<ResponseParamsFromServer> call = gerritAPI.mGetCitizenLoginDetailsConnect(requestParamsToServer);
            call.enqueue(this);

        }

        @Override
        public void onResponse(Call<ResponseParamsFromServer> call, Response<ResponseParamsFromServer> response) {

            ResponseParamsFromServer list = response.body();

            //store further use any where in application
            singleton.responseParamsFromServer = list;

            if(response.isSuccessful()) {

                Utility.printv("LoginServerController status code = " + list.STATUS_CODE);


            } else {
                //store further use any where in application
                singleton.responseParamsFromServer = null;
                Utility.printv(""+response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<ResponseParamsFromServer> call, Throwable t) {
            //store further use any where in application
            singleton.responseParamsFromServer = null;
            Utility.printv("=======failed===========");
            t.printStackTrace();
        }

    }// end controller for server to get data in json format



    public void GetReligionSpinner(RequestParamsToServer requestParamsToServer) throws Exception {


        //------------- @important default ----------

        // this proof no server call have been made
        ResponseParamsFromServer responseParamsFromServer = new ResponseParamsFromServer();
        responseParamsFromServer.STATUS_CODE=Constant.recursion_code;

        //by default response from this master
        singleton.responseParamsFromServer = responseParamsFromServer;

        //------------- @end default ----------


        MasterWebController.ReligionServerController controller = new MasterWebController.ReligionServerController();
        controller.start(requestParamsToServer);

    }


    public class ReligionServerController implements Callback<ResponseParamsFromServer> {

        Singleton singleton = Singleton.getInstance();

        public void start(RequestParamsToServer requestParamsToServer) {


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiCaller gerritAPI = retrofit.create(ApiCaller.class);

            Call<ResponseParamsFromServer> call = gerritAPI.mReligionConnect(requestParamsToServer);
            call.enqueue(this);

        }

        @Override
        public void onResponse(Call<ResponseParamsFromServer> call, Response<ResponseParamsFromServer> response) {

            ResponseParamsFromServer list = response.body();

            //store further use any where in application
            singleton.responseParamsFromServer = list;

            if(response.isSuccessful()) {

                Utility.printv("status code = " + list.STATUS_CODE);

                if (list.STATUS_CODE.equals("200")) {
                    for (ResponseParamsFromServer.WSPReligionRS rs : list.ReligionList ){
                        Utility.printv("ReligionList = "+rs.ReligionName);
                    }
                }


            } else {
                singleton.responseParamsFromServer = null;
                Utility.printv(""+response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<ResponseParamsFromServer> call, Throwable t) {

            Utility.printv("=======failed===========");
            singleton.responseParamsFromServer = null;
            t.printStackTrace();
        }

    }// end controller for server to get data in json format



    public void GetRelationSpinner(RequestParamsToServer requestParamsToServer) throws Exception {


        //------------- @important default ----------

        // this proof no server call have been made
        ResponseParamsFromServer responseParamsFromServer = new ResponseParamsFromServer();
        responseParamsFromServer.STATUS_CODE=Constant.recursion_code;

        //by default response from this master
        singleton.responseParamsFromServer = responseParamsFromServer;

        //------------- @end default ----------


        MasterWebController.RelationServerController controller = new MasterWebController.RelationServerController();
        controller.start(requestParamsToServer);

    }


    public class RelationServerController implements Callback<ResponseParamsFromServer> {

        Singleton singleton = Singleton.getInstance();

        public void start(RequestParamsToServer requestParamsToServer) {


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiCaller gerritAPI = retrofit.create(ApiCaller.class);

            Call<ResponseParamsFromServer> call = gerritAPI.mRelationTypeConnect(requestParamsToServer);
            call.enqueue(this);

        }

        @Override
        public void onResponse(Call<ResponseParamsFromServer> call, Response<ResponseParamsFromServer> response) {

            ResponseParamsFromServer list = response.body();

            //store further use any where in application
            singleton.responseParamsFromServer = list;

            if(response.isSuccessful()) {

                Utility.printv("status code = " + list.STATUS_CODE);

                if (list.STATUS_CODE.equals("200")) {
                    for (ResponseParamsFromServer.WSPRelationTypeRS rs : list.RelationTypeList){
                        Utility.printv("RelationTypeList = "+rs.RelationType);
                    }
                }


            } else {
                singleton.responseParamsFromServer = null;
                Utility.printv(""+response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<ResponseParamsFromServer> call, Throwable t) {

            Utility.printv("=======failed===========");
            singleton.responseParamsFromServer = null;
            t.printStackTrace();
        }

    }// end controller for server to get data in json format


    public void GetDistrictSpinner(RequestParamsToServer requestParamsToServer) throws Exception {


        //------------- @important default ----------

        // this proof no server call have been made
        ResponseParamsFromServer responseParamsFromServer = new ResponseParamsFromServer();
        responseParamsFromServer.STATUS_CODE=Constant.recursion_code;

        //by default response from this master
        singleton.responseParamsFromServer = responseParamsFromServer;

        //------------- @end default ----------

        MasterWebController.DistrictServerController controller = new MasterWebController.DistrictServerController();
        controller.start(requestParamsToServer);

    }


    public class DistrictServerController implements Callback<ResponseParamsFromServer> {

        Singleton singleton = Singleton.getInstance();

        public void start(RequestParamsToServer requestParamsToServer) {


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiCaller gerritAPI = retrofit.create(ApiCaller.class);

            Call<ResponseParamsFromServer> call = gerritAPI.mDistrictConnect(requestParamsToServer);
            call.enqueue(this);

        }

        @Override
        public void onResponse(Call<ResponseParamsFromServer> call, Response<ResponseParamsFromServer> response) {

            ResponseParamsFromServer list = response.body();

            //store further use any where in application
            singleton.responseParamsFromServer = list;

            if(response.isSuccessful()) {

                Utility.printv("status code = " + list.STATUS_CODE);

                if (list.STATUS_CODE.equals("200")) {
                    for (ResponseParamsFromServer.WSPDistrict rs : list.DistrictList){
                        Utility.printv("DistrictList = "+rs.DistrictName);
                    }
                }


            } else {
                singleton.responseParamsFromServer = null;
                Utility.printv(""+response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<ResponseParamsFromServer> call, Throwable t) {

            Utility.printv("=======failed===========");
            singleton.responseParamsFromServer = null;
            t.printStackTrace();
        }

    }// end controller for server to get data in json format


    public void GetPoliceStationSpinner(RequestParamsToServer requestParamsToServer) throws Exception {


        //------------- @important default ----------

        // this proof no server call have been made
        ResponseParamsFromServer responseParamsFromServer = new ResponseParamsFromServer();
        responseParamsFromServer.STATUS_CODE=Constant.recursion_code;

        //by default response from this master
        singleton.responseParamsFromServer = responseParamsFromServer;

        //------------- @end default ----------


        MasterWebController.PoliceStationServerController controller = new MasterWebController.PoliceStationServerController();
        controller.start(requestParamsToServer);

    }


    public class PoliceStationServerController implements Callback<ResponseParamsFromServer> {

        Singleton singleton = Singleton.getInstance();

        public void start(RequestParamsToServer requestParamsToServer) {


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiCaller gerritAPI = retrofit.create(ApiCaller.class);

            Call<ResponseParamsFromServer> call = gerritAPI.mPoliceStationConnect(requestParamsToServer);
            call.enqueue(this);

        }

        @Override
        public void onResponse(Call<ResponseParamsFromServer> call, Response<ResponseParamsFromServer> response) {

            ResponseParamsFromServer list = response.body();

            //store further use any where in application
            singleton.responseParamsFromServer = list;

            if(response.isSuccessful()) {

                Utility.printv("status code = " + list.STATUS_CODE);


                if (list.STATUS_CODE.equals("200")) {
                    for (ResponseParamsFromServer.WSPPoliceStation rs : list.PoliceStationList){
                        Utility.printv("PoliceStationList = "+rs.PoliceStationName);
                    }
                }


            } else {
                singleton.responseParamsFromServer = null;
                Utility.printv(""+response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<ResponseParamsFromServer> call, Throwable t) {

            Utility.printv("=======failed===========");
            singleton.responseParamsFromServer = null;
            t.printStackTrace();
        }

    }// end controller for server to get data in json format

    public void GetPersonList(RequestParamsToServer requestParamsToServer) throws Exception {


        //------------- @important default ----------

        // this proof no server call have been made
        ResponseParamsFromServer responseParamsFromServer = new ResponseParamsFromServer();
        responseParamsFromServer.STATUS_CODE=Constant.recursion_code;

        //by default response from this master
        singleton.responseParamsFromServer = responseParamsFromServer;

        //------------- @end default ----------

        MasterWebController.PersonSearchOnlyServerController controller = new MasterWebController.PersonSearchOnlyServerController();
        controller.start(requestParamsToServer);

    }


    public class PersonSearchOnlyServerController implements Callback<ResponseParamsFromServer> {

        Singleton singleton = Singleton.getInstance();

        public void start(RequestParamsToServer requestParamsToServer) {


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.API_BASE_URL)
                    .client(getClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiCaller gerritAPI = retrofit.create(ApiCaller.class);

            Call<ResponseParamsFromServer> call = gerritAPI.mPersonSearch(requestParamsToServer);
            call.enqueue(this);

        }

        @Override
        public void onResponse(Call<ResponseParamsFromServer> call, Response<ResponseParamsFromServer> response) {

            ResponseParamsFromServer list = response.body();

            //store further use any where in application
            singleton.responseParamsFromServer = list;

            if(response.isSuccessful()) {

                Utility.printv("status code = " + list.STATUS_CODE);

                if (list.STATUS_CODE.equals("200")) {
                    for (ResponseParamsFromServer.WSPPersonSearchRS rs : list.personSearchList){
                        Utility.printv("personSearchList  = "+rs.FULL_NAME);
                    }
                }

            } else {
                singleton.responseParamsFromServer = null;
                Utility.printv(""+response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<ResponseParamsFromServer> call, Throwable t) {

            Utility.printv("=======failed===========");
            singleton.responseParamsFromServer = null;
            t.printStackTrace();
        }

    }// end controller for server to get data in json format


    public void GetPropertyList(RequestParamsToServer requestParamsToServer) throws Exception {


        //------------- @important default ----------

        // this proof no server call have been made
        ResponseParamsFromServer responseParamsFromServer = new ResponseParamsFromServer();
        responseParamsFromServer.STATUS_CODE=Constant.recursion_code;

        //by default response from this master
        singleton.responseParamsFromServer = responseParamsFromServer;

        //------------- @end default ----------


        MasterWebController.PropertySearchOnlyServerController controller = new MasterWebController.PropertySearchOnlyServerController();
        controller.start(requestParamsToServer);

    }


    public class PropertySearchOnlyServerController implements Callback<ResponseParamsFromServer> {

        Singleton singleton = Singleton.getInstance();

        public void start(RequestParamsToServer requestParamsToServer) {


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.API_BASE_URL)
                    .client(getClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiCaller gerritAPI = retrofit.create(ApiCaller.class);

            Call<ResponseParamsFromServer> call = gerritAPI.mPropertySearch(requestParamsToServer);
            call.enqueue(this);

        }

        @Override
        public void onResponse(Call<ResponseParamsFromServer> call, Response<ResponseParamsFromServer> response) {

            ResponseParamsFromServer list = response.body();

            //store further use any where in application
            singleton.responseParamsFromServer = list;

            if(response.isSuccessful()) {

                Utility.printv("status code = " + list.STATUS_CODE);

                if (list.STATUS_CODE.equals("200")) {
                    for (ResponseParamsFromServer.WSPPropertySearchRS rs : list.propertySearchCheck){
                        Utility.printv("propertySearchCheck  = "+rs.registrationNo);
                    }
                }


            } else {
                singleton.responseParamsFromServer = null;
                Utility.printv(""+response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<ResponseParamsFromServer> call, Throwable t) {

            Utility.printv("=======failed===========");
            singleton.responseParamsFromServer = null;
            t.printStackTrace();
        }

    }// end controller for server to get data in json format


    public void GetVehicleTypeSpinner(RequestParamsToServer requestParamsToServer) throws Exception {


        //------------- @important default ----------

        // this proof no server call have been made
        ResponseParamsFromServer responseParamsFromServer = new ResponseParamsFromServer();
        responseParamsFromServer.STATUS_CODE=Constant.recursion_code;

        //by default response from this master
        singleton.responseParamsFromServer = responseParamsFromServer;

        //------------- @end default ----------

        MasterWebController.VehicleTypeServerController controller = new MasterWebController.VehicleTypeServerController();
        controller.start(requestParamsToServer);

    }


    public class VehicleTypeServerController implements Callback<ResponseParamsFromServer> {

        Singleton singleton = Singleton.getInstance();

        public void start(RequestParamsToServer requestParamsToServer) {


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiCaller gerritAPI = retrofit.create(ApiCaller.class);

            Call<ResponseParamsFromServer> call = gerritAPI.mVehicleTypeConnect(requestParamsToServer);
            call.enqueue(this);

        }

        @Override
        public void onResponse(Call<ResponseParamsFromServer> call, Response<ResponseParamsFromServer> response) {

            ResponseParamsFromServer list = response.body();

            //store further use any where in application
            singleton.responseParamsFromServer = list;

            if(response.isSuccessful()) {

                Utility.printv("status code = " + list.STATUS_CODE);

                if (list.STATUS_CODE.equals("200")) {
                    for (ResponseParamsFromServer.WSPVehicleRS rs : list.vehicleTypeCheck){
                        Utility.printv("vehicleTypeCheck "+rs.VehicleType);
                    }
                }


            } else {
                singleton.responseParamsFromServer = null;
                Utility.printv(""+response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<ResponseParamsFromServer> call, Throwable t) {

            Utility.printv("=======failed===========");
            singleton.responseParamsFromServer = null;
            t.printStackTrace();
        }

    }// end controller for server to get data in json format


    public void GetMVColorSpinner(RequestParamsToServer requestParamsToServer) throws Exception {


        //------------- @important default ----------

        // this proof no server call have been made
        ResponseParamsFromServer responseParamsFromServer = new ResponseParamsFromServer();
        responseParamsFromServer.STATUS_CODE=Constant.recursion_code;

        //by default response from this master
        singleton.responseParamsFromServer = responseParamsFromServer;

        //------------- @end default ----------

        MasterWebController.MVColorServerController controller = new MasterWebController.MVColorServerController();
        controller.start(requestParamsToServer);

    }


    public class MVColorServerController implements Callback<ResponseParamsFromServer> {

        Singleton singleton = Singleton.getInstance();

        public void start(RequestParamsToServer requestParamsToServer) {


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiCaller gerritAPI = retrofit.create(ApiCaller.class);

            Call<ResponseParamsFromServer> call = gerritAPI.mMVColorConnect(requestParamsToServer);
            call.enqueue(this);

        }

        @Override
        public void onResponse(Call<ResponseParamsFromServer> call, Response<ResponseParamsFromServer> response) {

            ResponseParamsFromServer list = response.body();

            //store further use any where in application
            singleton.responseParamsFromServer = list;

            if(response.isSuccessful()) {

                Utility.printv("status code = " + list.STATUS_CODE);

                if (list.STATUS_CODE.equals("200")) {
                    for (ResponseParamsFromServer.WSPMVColorRS rs : list.MVColorList){
                        Utility.printv("MVColorList = "+rs.MVColorName);
                    }
                }


            } else {
                singleton.responseParamsFromServer = null;
                Utility.printv(""+response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<ResponseParamsFromServer> call, Throwable t) {

            Utility.printv("=======failed===========");
            singleton.responseParamsFromServer = null;
            t.printStackTrace();
        }

    }// end controller for server to get data in json format


    public void GetMVModelSpinner(RequestParamsToServer requestParamsToServer) throws Exception {


        //------------- @important default ----------

        // this proof no server call have been made
        ResponseParamsFromServer responseParamsFromServer = new ResponseParamsFromServer();
        responseParamsFromServer.STATUS_CODE=Constant.recursion_code;

        //by default response from this master
        singleton.responseParamsFromServer = responseParamsFromServer;

        //------------- @end default ----------


        MasterWebController.MVModelServerController controller = new MasterWebController.MVModelServerController();
        controller.start(requestParamsToServer);

    }


    public class MVModelServerController implements Callback<ResponseParamsFromServer> {

        Singleton singleton = Singleton.getInstance();

        public void start(RequestParamsToServer requestParamsToServer) {


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiCaller gerritAPI = retrofit.create(ApiCaller.class);

            Call<ResponseParamsFromServer> call = gerritAPI.mMVModelConnect(requestParamsToServer);
            call.enqueue(this);

        }

        @Override
        public void onResponse(Call<ResponseParamsFromServer> call, Response<ResponseParamsFromServer> response) {

            ResponseParamsFromServer list = response.body();

            //store further use any where in application
            singleton.responseParamsFromServer = list;

            if(response.isSuccessful()) {

                Utility.printv("status code = " + list.STATUS_CODE);

                if (list.STATUS_CODE.equals("200")) {
                    for (ResponseParamsFromServer.WSPMVModelRS rs : list.MVModelList){
                        Utility.printv("MVModelList= "+rs.MVModelName);
                    }
                }


            } else {
                singleton.responseParamsFromServer = null;
                Utility.printv(""+response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<ResponseParamsFromServer> call, Throwable t) {

            Utility.printv("=======failed===========");
            singleton.responseParamsFromServer = null;
            t.printStackTrace();
        }

    }// end controller for server to get data in json format

    public void GetManufacturerSpinner(RequestParamsToServer requestParamsToServer) throws Exception {


        //------------- @important default ----------

        // this proof no server call have been made
        ResponseParamsFromServer responseParamsFromServer = new ResponseParamsFromServer();
        responseParamsFromServer.STATUS_CODE=Constant.recursion_code;

        //by default response from this master
        singleton.responseParamsFromServer = responseParamsFromServer;

        //------------- @end default ----------

        MasterWebController.ManufacturerServerController controller = new MasterWebController.ManufacturerServerController();
        controller.start(requestParamsToServer);

    }


    public class ManufacturerServerController implements Callback<ResponseParamsFromServer> {

        Singleton singleton = Singleton.getInstance();

        public void start(RequestParamsToServer requestParamsToServer) {


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiCaller gerritAPI = retrofit.create(ApiCaller.class);

            Call<ResponseParamsFromServer> call = gerritAPI.mManufacturerConnect(requestParamsToServer);
            call.enqueue(this);

        }

        @Override
        public void onResponse(Call<ResponseParamsFromServer> call, Response<ResponseParamsFromServer> response) {

            ResponseParamsFromServer list = response.body();

            //store further use any where in application
            singleton.responseParamsFromServer = list;

            if(response.isSuccessful()) {

                Utility.printv("status code = " + list.STATUS_CODE);

                if (list.STATUS_CODE.equals("200")) {
                    for (ResponseParamsFromServer.WSPManufacturerRS rs : list.ManufacturerList){
                        Utility.printv("ManufacturerList = "+rs.ManuName);
                    }
                }


            } else {
                singleton.responseParamsFromServer = null;
                Utility.printv(""+response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<ResponseParamsFromServer> call, Throwable t) {

            Utility.printv("=======failed===========");
            singleton.responseParamsFromServer = null;
            t.printStackTrace();
        }

    }// end controller for server to get data in json format


    public void GetFIRDetail(RequestParamsToServer requestParamsToServer) throws Exception {


        //------------- @important default ----------

        // this proof no server call have been made
        ResponseParamsFromServer responseParamsFromServer = new ResponseParamsFromServer();
        responseParamsFromServer.STATUS_CODE=Constant.recursion_code;

        //by default response from this master
        singleton.responseParamsFromServer = responseParamsFromServer;

        //------------- @end default ----------

        Gson gson = new Gson(); String json = gson.toJson(requestParamsToServer);
        Utility.ConvertObjectToArray(json,""+this.getClass().getSimpleName());

        MasterWebController.FIRDetailServerController controller = new MasterWebController.FIRDetailServerController();
        controller.start(requestParamsToServer);

    }


    public class FIRDetailServerController implements Callback<ResponseParamsFromServer> {

        Singleton singleton = Singleton.getInstance();

        public void start(RequestParamsToServer requestParamsToServer) {


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.API_BASE_URL)
                    .client(getClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiCaller gerritAPI = retrofit.create(ApiCaller.class);



            Call<ResponseParamsFromServer> call = gerritAPI.mGetFIRStatus(requestParamsToServer);
            call.enqueue(this);

        }

        @Override
        public void onResponse(Call<ResponseParamsFromServer> call, Response<ResponseParamsFromServer> response) {

            ResponseParamsFromServer list = response.body();

            //store further use any where in application
            singleton.responseParamsFromServer = list;

            if(response.isSuccessful()) {

                Utility.printv("status code FIR = " + list.STATUS_CODE);


            } else {
                singleton.responseParamsFromServer = null;
                Utility.printv(""+response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<ResponseParamsFromServer> call, Throwable t) {

            Utility.printv("=======failed===========");
            singleton.responseParamsFromServer = null;
            t.printStackTrace();
        }

    }// end controller for server to get data in json format


    public void GetMoreDetailPerson(RequestParamsToServer requestParamsToServer) throws Exception {


        //------------- @important default ----------

        // this proof no server call have been made
        ResponseParamsFromServer responseParamsFromServer = new ResponseParamsFromServer();
        responseParamsFromServer.STATUS_CODE=Constant.recursion_code;

        //by default response from this master
        singleton.responseParamsFromServer = responseParamsFromServer;

        //------------- @end default ----------

        MasterWebController.MoreDetailPersonServerController controller = new MasterWebController.MoreDetailPersonServerController();
        controller.start(requestParamsToServer);

    }


    public class MoreDetailPersonServerController implements Callback<ResponseParamsFromServer> {

        Singleton singleton = Singleton.getInstance();

        public void start(RequestParamsToServer requestParamsToServer) {


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiCaller gerritAPI = retrofit.create(ApiCaller.class);

            Utility.printv("m_service = " + requestParamsToServer.m_service);

            Call<ResponseParamsFromServer> call = gerritAPI.mPersonMoreDetails(requestParamsToServer);
            call.enqueue(this);

        }

        @Override
        public void onResponse(Call<ResponseParamsFromServer> call, Response<ResponseParamsFromServer> response) {

            ResponseParamsFromServer list = response.body();

            //store further use any where in application
            singleton.responseParamsFromServer = list;

            if(response.isSuccessful()) {

                Utility.printv("status code = " + list.STATUS_CODE);

                if (list.STATUS_CODE.equals("200")) {
                    for (ResponseParamsFromServer.WSPArrestCase rs : list.PersonMoreDetailsList){
                        Utility.printv("WSPArrestCase = "+rs.ArrestedResult);
                    }
                }

            } else {
                singleton.responseParamsFromServer = null;
                Utility.printv("=======failed 1===========");
                Utility.printv("response.errorBody "+response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<ResponseParamsFromServer> call, Throwable t) {

            Utility.printv("=======failed 2===========");
            singleton.responseParamsFromServer = null;
            t.printStackTrace();
        }

    }// end controller for server to get data in json format

    public void GetMoreDetailPersonChargesheet(RequestParamsToServer requestParamsToServer) throws Exception {


        //------------- @important default ----------

        // this proof no server call have been made
        ResponseParamsFromServer responseParamsFromServer = new ResponseParamsFromServer();
        responseParamsFromServer.STATUS_CODE=Constant.recursion_code;

        //by default response from this master
        singleton.responseParamsFromServer = responseParamsFromServer;

        //------------- @end default ----------

        MasterWebController.MoreDetailPersonChargesheetServerController controller = new MasterWebController.MoreDetailPersonChargesheetServerController();
        controller.start(requestParamsToServer);

    }


    public class MoreDetailPersonChargesheetServerController implements Callback<ResponseParamsFromServer> {

        Singleton singleton = Singleton.getInstance();

        public void start(RequestParamsToServer requestParamsToServer) {


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiCaller gerritAPI = retrofit.create(ApiCaller.class);

            Call<ResponseParamsFromServer> call = gerritAPI.mPersonMoreDetailsChargesheet(requestParamsToServer);
            call.enqueue(this);

        }

        @Override
        public void onResponse(Call<ResponseParamsFromServer> call, Response<ResponseParamsFromServer> response) {

            ResponseParamsFromServer list = response.body();

            //store further use any where in application
            singleton.responseParamsFromServer = list;

            if(response.isSuccessful()) {

                Utility.printv("status code = " + list.STATUS_CODE);

                if (list.STATUS_CODE.equals("200")) {
                    //todo change this for charge  sheet
                    for (ResponseParamsFromServer.WSPArrestCase rs : list.PersonMoreDetailsList){
                        Utility.printv("Change me Chargesheet = "+rs.ArrestedResult);
                    }
                }


            } else {
                singleton.responseParamsFromServer = null;
                Utility.printv(""+response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<ResponseParamsFromServer> call, Throwable t) {

            Utility.printv("=======failed===========");
            singleton.responseParamsFromServer = null;
            t.printStackTrace();
        }

    }// end controller for server to get data in json format


    public void GetMoreDetailPersonConvict(RequestParamsToServer requestParamsToServer) throws Exception {


        //------------- @important default ----------

        // this proof no server call have been made
        ResponseParamsFromServer responseParamsFromServer = new ResponseParamsFromServer();
        responseParamsFromServer.STATUS_CODE=Constant.recursion_code;

        //by default response from this master
        singleton.responseParamsFromServer = responseParamsFromServer;

        //------------- @end default ----------

        MasterWebController.MoreDetailPersonConvictServerController controller = new MasterWebController.MoreDetailPersonConvictServerController();
        controller.start(requestParamsToServer);

    }


    public class MoreDetailPersonConvictServerController implements Callback<ResponseParamsFromServer> {

        Singleton singleton = Singleton.getInstance();

        public void start(RequestParamsToServer requestParamsToServer) {


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiCaller gerritAPI = retrofit.create(ApiCaller.class);

            Call<ResponseParamsFromServer> call = gerritAPI.mPersonMoreDetailsConvict(requestParamsToServer);
            call.enqueue(this);

        }

        @Override
        public void onResponse(Call<ResponseParamsFromServer> call, Response<ResponseParamsFromServer> response) {

            ResponseParamsFromServer list = response.body();

            //store further use any where in application
            singleton.responseParamsFromServer = list;

            if(response.isSuccessful()) {

                Utility.printv("status code = " + list.STATUS_CODE);

                if (list.STATUS_CODE.equals("200")) {
                    for (ResponseParamsFromServer.WSPConvictCase rs : list.PersonMoreDetailsConvictList){
                        Utility.printv("PersonMoreDetailsConvictList = "+rs.IsConvicted);
                    }
                }


            } else {
                singleton.responseParamsFromServer = null;
                Utility.printv(""+response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<ResponseParamsFromServer> call, Throwable t) {

            Utility.printv("=======failed===========");
            singleton.responseParamsFromServer = null;
            t.printStackTrace();
        }

    }// end controller for server to get data in json format


    public void GetMoreDetailPersonPHOffender(RequestParamsToServer requestParamsToServer) throws Exception {


        //------------- @important default ----------

        // this proof no server call have been made
        ResponseParamsFromServer responseParamsFromServer = new ResponseParamsFromServer();
        responseParamsFromServer.STATUS_CODE=Constant.recursion_code;

        //by default response from this master
        singleton.responseParamsFromServer = responseParamsFromServer;

        //------------- @end default ----------

        MasterWebController.MoreDetailPersonPHOffenderServerController controller = new MasterWebController.MoreDetailPersonPHOffenderServerController();
        controller.start(requestParamsToServer);

    }


    public class MoreDetailPersonPHOffenderServerController implements Callback<ResponseParamsFromServer> {

        Singleton singleton = Singleton.getInstance();

        public void start(RequestParamsToServer requestParamsToServer) {


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiCaller gerritAPI = retrofit.create(ApiCaller.class);

            Call<ResponseParamsFromServer> call = gerritAPI.mPersonMoreDetailsPHOffender(requestParamsToServer);
            call.enqueue(this);

        }

        @Override
        public void onResponse(Call<ResponseParamsFromServer> call, Response<ResponseParamsFromServer> response) {

            ResponseParamsFromServer list = response.body();

            //store further use any where in application
            singleton.responseParamsFromServer = list;

            if(response.isSuccessful()) {

                Utility.printv("status code = " + list.STATUS_CODE);

                if (list.STATUS_CODE.equals("200")) {
                    for (ResponseParamsFromServer.WSPOffenderCase rs : list.PersonMoreDetailsPHOffenderList){
                        Utility.printv("PersonMoreDetailsPHOffenderList = "+rs.COURT_TYPE);
                    }
                }


            } else {
                singleton.responseParamsFromServer = null;
                Utility.printv(""+response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<ResponseParamsFromServer> call, Throwable t) {

            Utility.printv("=======failed===========");
            singleton.responseParamsFromServer = null;
            t.printStackTrace();
        }

    }// end controller for server to get data in json format

    public void GetAccusedImage(RequestParamsToServer requestParamsToServer) throws Exception {


        //------------- @important default ----------

        // this proof no server call have been made
        ResponseParamsFromServer responseParamsFromServer = new ResponseParamsFromServer();
        responseParamsFromServer.STATUS_CODE=Constant.recursion_code;

        //by default response from this master
        singleton.responseParamsFromServer = responseParamsFromServer;

        //------------- @end default ----------

        MasterWebController.AccusedImageServerController controller = new MasterWebController.AccusedImageServerController();
        controller.start(requestParamsToServer);

    }

    public class AccusedImageServerController implements Callback<ResponseParamsFromServer> {

        Singleton singleton = Singleton.getInstance();

        public void start(RequestParamsToServer requestParamsToServer) {


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiCaller gerritAPI = retrofit.create(ApiCaller.class);

            Call<ResponseParamsFromServer> call = gerritAPI.mGetImagesBlob(requestParamsToServer);
            call.enqueue(this);

        }

        @Override
        public void onResponse(Call<ResponseParamsFromServer> call, Response<ResponseParamsFromServer> response) {

            ResponseParamsFromServer list = response.body();

            //store further use any where in application
            singleton.resonsemGetImagesBlob = list;

            if(response.isSuccessful()) {

                Utility.printv("status code = " + list.STATUS_CODE);

            } else {
                singleton.resonsemGetImagesBlob = null;
                Utility.printv(""+response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<ResponseParamsFromServer> call, Throwable t) {

            Utility.printv("=======failed===========");
            singleton.resonsemGetImagesBlob = null;
            t.printStackTrace();
        }

    }// end controller for server to get data in json format



}// end json post params