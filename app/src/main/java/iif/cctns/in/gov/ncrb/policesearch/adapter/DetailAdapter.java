package iif.cctns.in.gov.ncrb.policesearch.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import iif.cctns.in.gov.ncrb.policesearch.R;


/**
 * Created by Vishnu
 */
public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.MyViewHolder> {

    Activity mContext;

    public DetailAdapter(Activity activity) {
        this.mContext = activity;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout llMain;

        public MyViewHolder(View view) {
            super(view);
            llMain = view.findViewById(R.id.llMain);

        }

    }// end MyViewHolder class...........


    // add single item view
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_two_section, parent, false);

        return new MyViewHolder(itemView);
    }

    // Used to set Tire
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if (position % 2 == 0)
            holder.llMain.setBackgroundColor(mContext.getResources().getColor(R.color.bg_color));
        else
            holder.llMain.setBackgroundColor(mContext.getResources().getColor(R.color.white));


    }


    @Override
    public int getItemCount() {
        return 20;
    }

}// end main class............................