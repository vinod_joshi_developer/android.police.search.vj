package iif.cctns.in.gov.ncrb.policesearch.utils;

/**
 * Vishnu
 */

public class Constant {

    public static int SPLASH_TIME_INTERVAL = 3000;

    //public static String API_BASE_URL = "http://192.168.0.101/policesearch/";
    public static String API_BASE_URL = "http://10.23.72.58:8080/CCTNSPoliceSearch/";

    public static String STACK_JAVA = "STACK_JAVA";

    public static String SearchTypePerson = "Person";

    public static String SearchTypeProperty = "Property";

    public static String STATE_CD = "27";// rajasthan=27,Har=13

    public static String LANG_CD = "99";// HINDI =6 ENGLISH=99

    public static String LANG_CD_HINDI = "6";// HINDI =6 ENGLISH=99

    public static String LANG_CD_ENG = "99";// HINDI =6 ENGLISH=99

    public static String recursion_code = "#55#55#55#";

    public static String SERVER_NO_DATA = "No data found or Server Error. Please try later";
    public static String SUCCESS_200 = "200";
    public static String FAIL_500 = "500";


    // web services names , list if create any new
    public static String mDistrictConnect = "mDistrictConnect";
    public static String mGetFIRStatus= "mGetFIRStatus";
    public static String mLoginVerify= "mGetCitizenLoginDetailsConnect";
    public static String mManufacturerConnect= "mManufacturerConnect";
    public static String mMVColorConnect= "mMVColorConnect";
    public static String mMVModelConnect= "mMVModelConnect";
    public static String mPersonMoreDetails= "mPersonMoreDetails";
    public static String mPersonMoreDetailsConvict= "mPersonMoreDetailsConvict";
    public static String mPersonMoreDetailsPHOffender= "mPersonMoreDetailsPHOffender";
    public static String mPersonSearch= "mPersonSearch";
    public static String mPoliceStationConnect= "mPoliceStationConnect";
    public static String mPropertySearch= "mPropertySearch";
    public static String mRelationTypeConnect= "mRelationTypeConnect";
    public static String mReligionConnect= "mReligionConnect";
    public static String mVehicleTypeConnect= "mVehicleTypeConnect";
    public static String mGetImagesBlob= "mGetImagesBlob";


}
