package iif.cctns.in.gov.ncrb.policesearch.webservice;


import android.support.annotation.Keep;

import java.util.List;


@Keep
public class ResponseParamsFromServer {


    // mLoginVerify
    public String STATUS_CODE;
    public String STATUS;
    public String MESSAGE;
    public String match;

    // FIR
    public String BASE64_BINARY;

    //image
    public String BASE64IMAGE;


    //mReligionConnect

    public List<WSPReligionRS> ReligionList;


    /**
     *
     *
     *
     */

    public class WSPReligionRS {

        public WSPReligionRS() {
        }

        public String ReligionCd;
        public String ReligionName;

    }

    public List<WSPRelationTypeRS> RelationTypeList;


    public List<WSPRelationTypeRS> getRelationTypeList() {
        return RelationTypeList;
    }

    public void setRelationTypeList(List<WSPRelationTypeRS> relationTypeList) {
        RelationTypeList = relationTypeList;
    }

    public class WSPRelationTypeRS {

        public WSPRelationTypeRS() {
        }


        public String RelationTypeCd;
        public String RelationType;


    }


    public List<WSPDistrict> DistrictList;


    /**
     *
     *
     *
     */

    public class WSPDistrict {

        public WSPDistrict() {
        }


        public String DistrictName;
        public String DistrictCd;


    }// end WSPDistrict


    public List<WSPPoliceStation> PoliceStationList;


    /**
     *
     *
     *
     */

    public class WSPPoliceStation {

        public WSPPoliceStation() {
        }


        public String PoliceStationName;
        public String PoliceStationCd;


    }// end WSPPoliceStation


    public List<WSPPersonSearchRS> personSearchList;


    public class WSPPersonSearchRS {

        public WSPPersonSearchRS() {
        }

//        public String FULL_NAME;
//        public String GENDER;
//        public String ALIAS1;
//        public String RELIGION;
//        public String RELATION_TYPE;
//        public String RELATIVE_NAME;
//        public String AGE;
//        public String HEIGHT_FROM_CM;
//        public String HEIGHT_TO_CM;
//        public String STATE_ENG;
//        public String DISTRICT;
//        public String PS;
//        public String MatchingCriteria;
//        public String REGISTRATION_NUM;
//        public String REG_DT;
//        public String REG_NUM;
//        public String UID_NUM;
//        public String PERMANENT;
//        public String PRESENT;
//        public String UploadedFile;
//        public String section_code;
//        public String all_accused_names;
//        public String accused_srno;
//        public String DISTRICT_CD;
//        public String PS_CD;
//        public String section;

        public String	accused_srno 	;
        public String	uid_num	;
        public String	REG_DT	;
        public String	REGISTRATION_NUM	;
        public String	REG_NUM	;
        public String	DOB	;
        public String	ARREST_SURR_DT	;
        public String	MatchingCriteria	;
        public String	SectionCd	;
        public String	Section	;
        public String	all_accused	;
        public String	FULL_NAME	;
        public String	STATE_ENG	;
        public String	district	;
        public String	ps	;
        public String	relation_type	;
        public String	gender	;
        public String	religion	;
        public String	RELATIVE_NAME	;
        public String	age	;
        public String	PERSON_CODE	;
        public String	lang_cd	;
        public String	ImageID	;
        public String	UploadedFile	;
        public String	PERMANENT	;
        public String	PRESENT	;
        public String	alias1	;
        public String	height_to_cm	;
        public String	height_from_cm	;
        public String	STATE	;
        public String	accusedsrNo	;
        public String	state_cd	;
        public String	district_cd	;
        public String	ps_cd	;



    }

    public List<WSPVehicleRS> vehicleTypeCheck;


    public class WSPVehicleRS {

        public WSPVehicleRS() {
        }


        public String VehicleType;
        public String VehicleTypecd;


    }

    public List<WSPMVColorRS> MVColorList;


    public class WSPMVColorRS {

        public WSPMVColorRS() {
        }


        public String MVColorCd;
        public String MVColorName;


    }


    public List<WSPManufacturerRS> ManufacturerList;


    public class WSPManufacturerRS {

        public WSPManufacturerRS() {
        }


        public String ManuName;
        public String ManuCd;


    }

    public List<WSPMVModelRS> MVModelList;


    public class WSPMVModelRS {

        public WSPMVModelRS() {
        }


        public String MVModelCd;
        public String MVModelName;


    }


    public List<WSPPropertySearchRS> propertySearchCheck;


    public class WSPPropertySearchRS {

        public WSPPropertySearchRS() {
        }

        public String 	reg_dt	;
        public String 	firRegNum	;
        public String 	fullfirnumber	;
        public String 	GD_No	;
        public String 	firstName	;
        public String 	STATE	;
        public String 	district	;
        public String 	ps	;
        public String 	propertyNature	;
        public String 	registrationNo	;
        public String 	mvType	;
        public String 	mvModel	;
        public String 	mvMake	;
        public String 	mvColor	;
        public String 	chasisNo	;
        public String 	engineNo	;
        public String 	Matching_Criteria	;
        public String 	state_cd	;
        public String 	district_cd	;
        public String 	ps_cd	;


    }

    public List<WSPArrestCase> PersonMoreDetailsList;


    public class WSPArrestCase {

        public WSPArrestCase() {
        }

        // this is for check if we have A and S type data
        public String arrest_surrend;
        public String ArrestedResult;
        public String ARREST_SURRENDER_DT;
        public String ARR_SURR_STATE_CD;
        public String ARR_SURR_DISTRICT_CD;
        public String occupation;
        public String ARR_SURR_PS_CD;
        public String SectionCode;
        public String section;
        public String SURREND_IN_COURT_CD;
        public String SURREND_MAGISTRATE;
        public String STATE_ENG;
        public String DISTRICT;
        public String PS;
        public String ARREST_ACTION;
        public String SURRENDERED_COURT;

    }

    public List<WSPConvictCase> PersonMoreDetailsConvictList;


    public class WSPConvictCase {

        public WSPConvictCase() {
        }

        // for convict only
        public String PUNISHMENT_TYPE_CD;
        public String PUNISHMENT_TYPE;
        public String judgement_date;
        public String PUNISH_YRS;
        public String PUNISH_MNTH;
        public String PUNISH_DAYS;
        public String IsConvicted;

    }

    public List<WSPOffenderCase> PersonMoreDetailsPHOffenderList;


    public class WSPOffenderCase {

        public WSPOffenderCase() {
        }

        public String PROCL_OFFENDER_COURT_TYPE;
        public String PROCL_OFFENDER_COURT_NAME;
        public String PROCL_OFFENDER_COURT_LOCATION;
        public String PROCL_OFFENDER_ORDER_NUM;
        public String PROCL_OFFENDER_ORDER_DT;
        public String COURT_TYPE;
        public String is_proclaimed_offender;
        public String HabitualOffender;

    }

}// end main class

