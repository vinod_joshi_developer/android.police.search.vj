package iif.cctns.in.gov.ncrb.policesearch;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

import iif.cctns.in.gov.ncrb.policesearch.adapter.PersonSearchResultAdapter;
import iif.cctns.in.gov.ncrb.policesearch.utils.Constant;
import iif.cctns.in.gov.ncrb.policesearch.utils.Singleton;
import iif.cctns.in.gov.ncrb.policesearch.utils.Utility;
import iif.cctns.in.gov.ncrb.policesearch.webservice.MasterWebController;
import okhttp3.OkHttpClient;

public class PersonSearchResultActivity extends AppCompatActivity {
    private Activity _acActivity = PersonSearchResultActivity.this;
    RecyclerView rvList;
    private TextView tvHeaderName;

    ProgressDialog mProgressDialog;
    Singleton singleton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_person_result);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Initalize all widgets
        initView();



    }//end onCreate-------------

    private void initView() {

        singleton = Singleton.getInstance();

        tvHeaderName = findViewById(R.id.tvHeaderName);
        tvHeaderName.setText("Person Search Result");


        findViewById(R.id.btn_previous).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(singleton.pagination_counter==0) {
                    Utility.showMessage("No Page",PersonSearchResultActivity.this);
                    return;
                }
                else singleton.pagination_counter = singleton.pagination_counter-15;

                loadPersonData();

                //Utility.showMessage("Next",PersonSearchResultActivity.this);
            }
        });

        findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (singleton.responseParamsFromServer==null) {

                    Utility.showMessage(Constant.SERVER_NO_DATA,PersonSearchResultActivity.this);
                    return;
                }

                if(singleton.responseParamsFromServer.STATUS_CODE.equals("500")) {
                    Utility.showMessage(Constant.SERVER_NO_DATA,PersonSearchResultActivity.this);
                } else {
                    singleton.pagination_counter = singleton.pagination_counter+15;
                    loadPersonData();
                    //Utility.showMessage("Next",PersonSearchResultActivity.this);
                }

            }
        });


        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        mProgressDialog = new ProgressDialog(PersonSearchResultActivity.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");




    }//end initView------------



    @Override
    public void onStart() {
        super.onStart();

        // start loading on create becuase it should not reload the page else network connect again
        loadPersonData();


    }// end onStart

    public void loadPersonData() {

        try {

            singleton.personSearchFormActivityInput.pagination_counter = ""+singleton.pagination_counter;

            mProgressDialog.show();
            MasterWebController masterWebController = new MasterWebController();
            masterWebController.GetPersonList(singleton.personSearchFormActivityInput);
            recursionMasterCheckPerson();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * kindly be careful recursion may be infinite if not handled carefully
     */
    private void recursionMasterCheckPerson() {

        if (singleton.responseParamsFromServer == null) {
            mProgressDialog.dismiss();
            Utility.showMessage(Constant.SERVER_NO_DATA, PersonSearchResultActivity.this);
            return;
        }

        if (!singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.recursion_code)) {

            if(singleton.responseParamsFromServer.STATUS_CODE.equals("200")){
                mProgressDialog.dismiss(); setPersonResult();
            } else {
                mProgressDialog.dismiss(); Utility.showMessage(Constant.SERVER_NO_DATA, PersonSearchResultActivity.this);
                revertPagination();
            }

        } else {

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    recursionMasterCheckPerson();
                }
            }, 2000); // After 5 seconds
        }
    }

    // @important revert pagination if no data found, else detail will not load

    public void revertPagination() {

        AlertDialog.Builder builder = new AlertDialog.Builder(PersonSearchResultActivity.this);
        builder.setMessage(Constant.SERVER_NO_DATA)
                .setPositiveButton("OKAY", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(singleton.pagination_counter==0) {
                            Utility.showMessage("No Page",PersonSearchResultActivity.this);
                            return;
                        }
                        else {
                            singleton.pagination_counter = singleton.pagination_counter - 15;
                            loadPersonData();
                        }
                    }
                });
        builder.show();
    }


    private void setPersonResult() {

        rvList = findViewById(R.id.rvList);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(PersonSearchResultActivity.this, LinearLayoutManager.VERTICAL, false);
        rvList.setLayoutManager(mLayoutManager);
        rvList.setItemAnimator(new DefaultItemAnimator());
        //rvList.setAdapter(new PersonSearchResultAdapter(PersonSearchResultActivity.this));
        rvList.setAdapter(new PersonSearchResultAdapter(PersonSearchResultActivity.this, singleton.responseParamsFromServer.personSearchList));



    }// end setPersonResult

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Utility.doAnim(_acActivity, "left");
    }



}//end main class--------------------
