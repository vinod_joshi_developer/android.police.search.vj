package iif.cctns.in.gov.ncrb.policesearch;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import iif.cctns.in.gov.ncrb.policesearch.utils.Constant;
import iif.cctns.in.gov.ncrb.policesearch.utils.Singleton;
import iif.cctns.in.gov.ncrb.policesearch.utils.Utility;
import iif.cctns.in.gov.ncrb.policesearch.webservice.MasterWebController;
import iif.cctns.in.gov.ncrb.policesearch.webservice.RequestParamsToServer;
import iif.cctns.in.gov.ncrb.policesearch.webservice.ResponseParamsFromServer;


public class FIROverView extends AppCompatActivity {


    AlertDialog alertDialog;

    ArrayList<String> YearValue = new ArrayList<>();

    int thisYear;

    File filePath;
    String nameapp = null;
    boolean ispdfexist;
    String district = null, ps = null;


    public ProgressDialog mProgressDialog;
    Singleton singleton;

    TextView txtview_folder_locs;

    RequestParamsToServer requestParamsToServer;
    ResponseParamsFromServer.WSPPersonSearchRS wspPersonSearchRS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fir_inputs);

        singleton = Singleton.getInstance();

        requestParamsToServer = new RequestParamsToServer();

        this.wspPersonSearchRS = singleton.responseParamsFromServer.personSearchList.get(singleton.personSearchRowNo);


        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");

        txtview_folder_locs = (TextView) findViewById(R.id.txtview_folder_locs);


        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();


        if (networkInfo != null && networkInfo.isConnected()) {


            String appendstring = null;

            try {

                if (singleton.FIR_firregnum.length() == 4) {
                    nameapp = singleton.FIR_firregnum + singleton.FIR_district_cd + singleton.FIR_ps_cd;
                } else if (singleton.FIR_firregnum.length() == 3) {
                    appendstring = "0" + singleton.FIR_firregnum;
                    nameapp = appendstring + singleton.FIR_district_cd + singleton.FIR_ps_cd;
                } else if (singleton.FIR_firregnum.length() == 2) {
                    appendstring = "0" + "0" + singleton.FIR_firregnum;
                    nameapp = appendstring + singleton.FIR_district_cd + singleton.FIR_ps_cd;
                } else if (singleton.FIR_firregnum.length() == 1) {
                    appendstring = "0" + "0" + "0" + singleton.FIR_firregnum;
                    nameapp = appendstring + singleton.FIR_district_cd + singleton.FIR_ps_cd;
                }

                Utility.printv("PDF Name " + nameapp + " FIR num " + singleton.FIR_firregnum);


                checkFIRDetail();


            } catch (Exception e) {
                Utility.printv("Error while parsing data.!! Please try later.");
            }


        } else {

            Utility.showMessage("Network failed", FIROverView.this);

        }


    }// end oncreate

    public void checkFIRDetail() {

        try {

            mProgressDialog.show();
            MasterWebController masterWebController = new MasterWebController();
            requestParamsToServer.m_service = Constant.mGetFIRStatus;


//            requestParamsToServer.FIR_firregnum = singleton.FIR_firregnum;
//            requestParamsToServer.FIR_district_cd = singleton.FIR_district_cd;
//            requestParamsToServer.FIR_ps_cd = singleton.FIR_ps_cd;
//            requestParamsToServer.FIR_year = singleton.FIR_year;


            String[] year_cum_fir = this.wspPersonSearchRS.REG_DT.split("/");

            requestParamsToServer.FIR_firregnum = this.wspPersonSearchRS.REGISTRATION_NUM;
            requestParamsToServer.FIR_district_cd = this.wspPersonSearchRS.district_cd;
            requestParamsToServer.FIR_ps_cd = this.wspPersonSearchRS.ps_cd;
            requestParamsToServer.FIR_year = year_cum_fir[1];


            masterWebController.GetFIRDetail(requestParamsToServer);
            recursionMasterCheck();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /**
     * kindly be careful recursion may be infinite if not handled carefully
     */
    private void recursionMasterCheck() {

        try {

            if (singleton.responseParamsFromServer == null) {
                mProgressDialog.dismiss();
                Utility.showMessage(Constant.SERVER_NO_DATA, FIROverView.this);
                return;
            }

            if (!singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.recursion_code)) {

                mProgressDialog.dismiss();

                if (singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.SUCCESS_200)) {
                    showFIRtoUser(singleton.responseParamsFromServer.BASE64_BINARY);
                } else {
                    Utility.showMessage(Constant.SERVER_NO_DATA, FIROverView.this);
                }


            } else {

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        recursionMasterCheck();
                    }
                }, 2000); // After 5 seconds
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * @ get district and ps list
     */

    public void showFIRtoUser(String result) throws Exception {

        if (result == null) {

            Utility.showMessage("Error while trying to fetch your FIR request.", FIROverView.this);

        } else {


            try {
                if (result.equals("ZWZk")) {

                    txtview_folder_locs.setText("FIR Not Found Or FIR is restricted to view as per supreme court order." + filePath);
                    Utility.showMessage("FIR Not Found Or FIR is restricted to view as per supreme court order.", FIROverView.this);


                } else if (result.equals("YWJjZA==")) {

                    txtview_folder_locs.setText("FIR Not Found Or FIR is restricted to view as per supreme court order." + filePath);
                    Utility.showMessage("FIR Not Found Or FIR is restricted to view as per supreme court order.", FIROverView.this);


                } else if (ispdfexist == true) {


                    txtview_folder_locs.setText("FIR file already exist. Please check your download folder." + filePath);
                    Utility.showMessage("FIR file already exist. Please check your download folder.", FIROverView.this);

                } else {

                    {

                        byte[] pdfAsBytes = Base64.decode(result.toString(), 0);

                        File direct = new File(Environment.getExternalStorageDirectory() + "/FIR Folder");

                        if (!direct.exists()) {

                            direct.mkdir();

                            filePath = new File(direct, File.separator + "FIR_no_" + nameapp + ".pdf");

                            System.out.println("filepath " + filePath);

                            if (filePath.isFile() == true) {

                                txtview_folder_locs.setText("File is already exist" + filePath);
                                Log.v("MSG", "File is already exist");
                                ispdfexist = filePath.isFile();

                            } else {

                                try {

                                    filePath.createNewFile();

                                    FileOutputStream fo = new FileOutputStream(filePath.getPath());

                                    fo.write(pdfAsBytes);

                                    fo.flush();

                                    fo.close();

                                } catch (Exception exc) {
                                    exc.printStackTrace();
                                }
                            }

                        } else {
                            filePath = new File(direct, File.separator + "FIR_no_" + nameapp + ".pdf");
                            System.out.println("filepath " + filePath);
                            if (filePath.isFile() == true) {
                                txtview_folder_locs.setText("File is already exist" + filePath);
                                Log.v("MSG", "File is already exist");
                                ispdfexist = filePath.isFile();
                            } else {
                                try {
                                    filePath.createNewFile();
                                    FileOutputStream fo = new FileOutputStream(filePath.getPath());
                                    fo.write(pdfAsBytes);
                                    fo.flush();
                                    fo.close();
                                } catch (Exception exc) {
                                    exc.printStackTrace();
                                }
                            }
                        }
                    }


                    txtview_folder_locs.setText("FIR in PDF has been downloaded in Device storage  \n Location: " + filePath);

                    alertDialog = new AlertDialog.Builder(FIROverView.this).create();
                    alertDialog.setTitle("");
                    alertDialog.setMessage("FIR in PDF has been downloaded in Device storage FIR Folder");
                    //alertDialog.setIcon(R.drawable.ncrb_logo_trans_55);
                    alertDialog.setButton("OK..", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            if (filePath.exists()) {
                                Uri filepath = Uri.fromFile(filePath);
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setDataAndType(filepath, "application/pdf");
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                try {
                                    startActivity(intent);
                                } catch (Exception e) {
                                    Log.e("error", "" + e);
                                }

                            } else {
                                txtview_folder_locs.setText("There is some problem" + filePath);

                                Log.e("else", "There is some problem");
                            }
                        }
                    });
                    alertDialog.show();
                }
            } catch (Exception e) {

                e.printStackTrace();

                alertDialog = new AlertDialog.Builder(FIROverView.this).create();
                alertDialog.setTitle("Attention...");
                alertDialog.setMessage("Get Error while fetching PDF...");
                //alertDialog.setIcon(R.drawable.ncrb_logo_trans_55);
                alertDialog.setButton("OK..", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // here you can add functions
                    }
                });
                alertDialog.show();
            }

        }// end else

    }// end show fir to user

}// end main class

