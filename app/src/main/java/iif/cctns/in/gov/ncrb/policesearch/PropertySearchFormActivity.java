package iif.cctns.in.gov.ncrb.policesearch;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import iif.cctns.in.gov.ncrb.policesearch.pojo.KeyValueSpinnerPojo;
import iif.cctns.in.gov.ncrb.policesearch.utils.Constant;
import iif.cctns.in.gov.ncrb.policesearch.utils.Singleton;
import iif.cctns.in.gov.ncrb.policesearch.utils.Utility;
import iif.cctns.in.gov.ncrb.policesearch.webservice.MasterWebController;
import iif.cctns.in.gov.ncrb.policesearch.webservice.RequestParamsToServer;
import iif.cctns.in.gov.ncrb.policesearch.webservice.ResponseParamsFromServer;

public class PropertySearchFormActivity extends AppCompatActivity {

    private Activity _acActivity = PropertySearchFormActivity.this;
    private TextView mTvHeader;


    Spinner sp_gender, sp_relation_Type, sp_religion, sp_district, sp_police_station;
    Spinner sp_Type_Vehicle,sp_Manufacturer,sp_color,sp_model;


    Singleton singleton;
    ProgressDialog mProgressDialog;

    String Dist_Code, PS_Code, mv_vehicle_cd, mv_manufacturer_cd, mv_model_cd, mv_color_cd; // district  , police station
    RequestParamsToServer requestParamsToServer;

    CheckBox chkFIR;
    CheckBox chkUAP;
    CheckBox chkPS;

    EditText edt_Reg_Num;
    EditText edt_Chasis_Num;
    EditText edt_Engine_Num;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_property);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Initalize all widgets
        initView();

    }//end onCreate-------------

    private void initView() {

        singleton = Singleton.getInstance();

        requestParamsToServer = new RequestParamsToServer();

        mTvHeader = findViewById(R.id.tvHeaderName);
        mTvHeader.setText("Property Search");

        sp_district = (Spinner) findViewById(R.id.sp_district);
        sp_police_station = (Spinner) findViewById(R.id.sp_police_station);

        chkUAP=(CheckBox) findViewById(R.id.chkUAP);
        chkPS=(CheckBox) findViewById(R.id.chkPS);
        chkFIR=(CheckBox) findViewById(R.id.chkFIR);

        edt_Reg_Num = (EditText) findViewById(R.id.edt_Reg_Num);
        edt_Chasis_Num = (EditText) findViewById(R.id.edt_Chasis_Num);
        edt_Engine_Num = (EditText) findViewById(R.id.edt_Engine_Num);


        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        findViewById(R.id.tvSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PropertySearchFormActivity.this.ValidateInput();
            }
        });


        // reset pagination counter
        singleton.pagination_counter=0;

        mProgressDialog = new ProgressDialog(PropertySearchFormActivity.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");

        sp_Type_Vehicle = (Spinner) findViewById(R.id.sp_Type_Vehicle);
        sp_Manufacturer = (Spinner) findViewById(R.id.sp_Manufacturer);
        sp_color = (Spinner) findViewById(R.id.sp_color);
        sp_model = (Spinner) findViewById(R.id.sp_model);


        chkUAP=(CheckBox) findViewById(R.id.chkUAP);
        chkPS=(CheckBox) findViewById(R.id.chkPS);
        chkFIR=(CheckBox) findViewById(R.id.chkFIR);

        // load content one time not to reload on back
        try {

            mProgressDialog.show();
            MasterWebController masterWebController = new MasterWebController();
            this.requestParamsToServer.m_service = Constant.mVehicleTypeConnect;
            masterWebController.GetVehicleTypeSpinner(this.requestParamsToServer);
            recursionMasterCheckVehicleType();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }//end initView------------





    @Override
    public void onStart() {
        super.onStart();




    }


    /**
     * kindly be careful recursion may be infinite if not handled carefully
     */
    private void recursionMasterCheckDistrict() {


        if (singleton.responseParamsFromServer.STATUS_CODE==null) {
            mProgressDialog.dismiss();
            Utility.showMessage(Constant.SERVER_NO_DATA,PropertySearchFormActivity.this);
            return;
        }

        if (!singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.recursion_code)) {


            mProgressDialog.dismiss();
            if (singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.SUCCESS_200)) {
                setDistrictData();
            }else{
                //Utility.showMessage(Constant.SERVER_NO_DATA,PropertySearchFormActivity.this);
            }


        } else {

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    recursionMasterCheckDistrict();
                }
            }, 2000); // After 2 seconds
        }
    }



    private void setDistrictData() {


        ArrayList<KeyValueSpinnerPojo> districtlist = new ArrayList<>();


        districtlist.add(new KeyValueSpinnerPojo("", "--Select District--"));

        for (ResponseParamsFromServer.WSPDistrict rs : singleton.responseParamsFromServer.DistrictList) {
            districtlist.add(new KeyValueSpinnerPojo(rs.DistrictCd, rs.DistrictName));
        }


        //fill data in spinner
        ArrayAdapter<KeyValueSpinnerPojo> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, districtlist);
        sp_district.setAdapter(adapter);
        sp_district.setSelection(0);//Optional to set the selected item.

        sp_district.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                KeyValueSpinnerPojo district = (KeyValueSpinnerPojo) parent.getSelectedItem();
                //Utility.showMessage("district ID: " + district.getId() + ",  district Name : " + district.getName(), PropertySearchFormActivity.this);
                PropertySearchFormActivity.this.Dist_Code = district.getId();

                //
                try {

                    mProgressDialog.show();
                    MasterWebController masterWebController = new MasterWebController();
                    PropertySearchFormActivity.this.requestParamsToServer.m_service = Constant.mPoliceStationConnect;
                    PropertySearchFormActivity.this.requestParamsToServer.District_Cd = district.getId();// todo change default
                    masterWebController.GetPoliceStationSpinner(PropertySearchFormActivity.this.requestParamsToServer);
                    recursionMasterCheckPoliceStation();

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }

    /**
     * kindly be careful recursion may be infinite if not handled carefully
     */
    private void recursionMasterCheckPoliceStation() {


        if (singleton.responseParamsFromServer.STATUS_CODE==null) {
            mProgressDialog.dismiss();
            Utility.showMessage(Constant.SERVER_NO_DATA,PropertySearchFormActivity.this);
            return;
        }

        if (!singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.recursion_code)) {

            mProgressDialog.dismiss();

            if (singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.SUCCESS_200)) {
                setPoliceStationData();
            }else{
                //Utility.showMessage(Constant.SERVER_NO_DATA,PropertySearchFormActivity.this);
            }


        } else {

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    recursionMasterCheckPoliceStation();
                }
            }, 2000); // After 2 seconds
        }
    }

    private void setPoliceStationData() {


        ArrayList<KeyValueSpinnerPojo> policestationlist = new ArrayList<>();


        policestationlist.add(new KeyValueSpinnerPojo("", "--Select Police Station--"));

        for (ResponseParamsFromServer.WSPPoliceStation rs : singleton.responseParamsFromServer.PoliceStationList) {
            policestationlist.add(new KeyValueSpinnerPojo(rs.PoliceStationCd, rs.PoliceStationName));
        }


        //fill data in spinner
        ArrayAdapter<KeyValueSpinnerPojo> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, policestationlist);
        sp_police_station.setAdapter(adapter);
        sp_police_station.setSelection(0);//Optional to set the selected item.

        sp_police_station.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                KeyValueSpinnerPojo policestation = (KeyValueSpinnerPojo) parent.getSelectedItem();
                //Utility.showMessage("policestation ID: " + policestation.getId() + ",  policestation Name : " + policestation.getName(), PropertySearchFormActivity.this);
                PropertySearchFormActivity.this.PS_Code = policestation.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }


    /**
     * kindly be careful recursion may be infinite if not handled carefully
     */
    private void recursionMasterCheckVehicleType() {


        if (singleton.responseParamsFromServer.STATUS_CODE==null) {
            mProgressDialog.dismiss();
            Utility.showMessage(Constant.SERVER_NO_DATA,PropertySearchFormActivity.this);
            return;
        }

        if (!singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.recursion_code)) {

            mProgressDialog.dismiss();

            if (singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.SUCCESS_200)) {
                setVehicleTypeData();
            }else{
                //Utility.showMessage(Constant.SERVER_NO_DATA,PropertySearchFormActivity.this);
            }



        } else {

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    recursionMasterCheckVehicleType();
                }
            }, 2000); // After 2 seconds
        }
    }



    private void setVehicleTypeData() {


        ArrayList<KeyValueSpinnerPojo> vehicleTypelist = new ArrayList<>();


        vehicleTypelist.add(new KeyValueSpinnerPojo("", "--Select Vehicle--"));

        for (ResponseParamsFromServer.WSPVehicleRS rs : singleton.responseParamsFromServer.vehicleTypeCheck) {
            vehicleTypelist.add(new KeyValueSpinnerPojo(rs.VehicleTypecd, rs.VehicleType));
        }


        //fill data in spinner
        ArrayAdapter<KeyValueSpinnerPojo> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, vehicleTypelist);
        sp_Type_Vehicle.setAdapter(adapter);
        sp_Type_Vehicle.setSelection(0);//Optional to set the selected item.

        sp_Type_Vehicle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                KeyValueSpinnerPojo vehicleType = (KeyValueSpinnerPojo) parent.getSelectedItem();
                //Utility.showMessage("vehicleType ID: " + vehicleType.getId() + ",  vehicleType Name : " + vehicleType.getName(), PropertySearchFormActivity.this);
                PropertySearchFormActivity.this.mv_vehicle_cd = vehicleType.getId();// todo change default value

                //
                try {

                    mProgressDialog.show();
                    MasterWebController masterWebController = new MasterWebController();
                    PropertySearchFormActivity.this.requestParamsToServer.m_service = Constant.mManufacturerConnect;
                    PropertySearchFormActivity.this.requestParamsToServer.VType_Cd = vehicleType.getId();
                    masterWebController.GetManufacturerSpinner(PropertySearchFormActivity.this.requestParamsToServer);
                    recursionMasterCheckManufacturer();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }

    /**
     * kindly be careful recursion may be infinite if not handled carefully
     */
    private void recursionMasterCheckManufacturer() {


        if (singleton.responseParamsFromServer.STATUS_CODE==null) {
            mProgressDialog.dismiss();
            Utility.showMessage(Constant.SERVER_NO_DATA,PropertySearchFormActivity.this);
            return;
        }

        if (!singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.recursion_code)) {

            mProgressDialog.dismiss();

            if (singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.SUCCESS_200)) {
                setManufacturerData();
            }else{
                LoadDistrict();
                //Utility.showMessage(Constant.SERVER_NO_DATA,PropertySearchFormActivity.this);
            }

        } else {

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    recursionMasterCheckManufacturer();
                }
            }, 2000); // After 2 seconds
        }
    }


    private void LoadDistrict() {

        // get all relation after getting religions
        try {

            mProgressDialog.show();
            MasterWebController masterWebController = new MasterWebController();
            requestParamsToServer.m_service = Constant.mDistrictConnect;
            requestParamsToServer.FxdState_Cd = Constant.STATE_CD;
            masterWebController.GetDistrictSpinner(requestParamsToServer);
            recursionMasterCheckDistrict();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setManufacturerData() {


        ArrayList<KeyValueSpinnerPojo> manufacturerlist = new ArrayList<>();


        manufacturerlist.add(new KeyValueSpinnerPojo("", "--Select Manufacturer--"));

        for (ResponseParamsFromServer.WSPManufacturerRS rs : singleton.responseParamsFromServer.ManufacturerList) {
            manufacturerlist.add(new KeyValueSpinnerPojo(rs.ManuCd, rs.ManuName));
        }


        //fill data in spinner
        ArrayAdapter<KeyValueSpinnerPojo> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, manufacturerlist);
        sp_Manufacturer.setAdapter(adapter);
        sp_Manufacturer.setSelection(0);//Optional to set the selected item.

        sp_Manufacturer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                KeyValueSpinnerPojo manufacturer = (KeyValueSpinnerPojo) parent.getSelectedItem();
                //Utility.showMessage("manufacturer ID: " + manufacturer.getId() + ",  manufacturer Name : " + manufacturer.getName(), PropertySearchFormActivity.this);
                PropertySearchFormActivity.this.mv_manufacturer_cd = manufacturer.getId();

                //
                try {

                    mProgressDialog.show();
                    MasterWebController masterWebController = new MasterWebController();
                    PropertySearchFormActivity.this.requestParamsToServer.m_service = Constant.mMVModelConnect;
                    PropertySearchFormActivity.this.requestParamsToServer.MVMake = manufacturer.getId();//todo change
                    masterWebController.GetMVModelSpinner(PropertySearchFormActivity.this.requestParamsToServer);
                    recursionMasterCheckMVModel();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }


    /**
     * kindly be careful recursion may be infinite if not handled carefully
     */
    private void recursionMasterCheckMVModel() {


        if (singleton.responseParamsFromServer.STATUS_CODE==null) {
            mProgressDialog.dismiss();
            Utility.showMessage(Constant.SERVER_NO_DATA,PropertySearchFormActivity.this);
            return;
        }

        if (!singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.recursion_code)) {

            mProgressDialog.dismiss();

            if (singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.SUCCESS_200)) {
                setMVModelData();
            }else{
                //Utility.showMessage(Constant.SERVER_NO_DATA,PropertySearchFormActivity.this);
            }


        } else {

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    recursionMasterCheckMVModel();
                }
            }, 2000); // After 2 seconds
        }
    }



    private void setMVModelData() {


        ArrayList<KeyValueSpinnerPojo> mvModellist = new ArrayList<>();


        mvModellist.add(new KeyValueSpinnerPojo("", "--Select Model--"));

        for (ResponseParamsFromServer.WSPMVModelRS rs : singleton.responseParamsFromServer.MVModelList) {
            mvModellist.add(new KeyValueSpinnerPojo(rs.MVModelCd, rs.MVModelName));
        }


        //fill data in spinner
        ArrayAdapter<KeyValueSpinnerPojo> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, mvModellist);
        sp_model.setAdapter(adapter);
        sp_model.setSelection(0);//Optional to set the selected item.

        sp_model.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                KeyValueSpinnerPojo mvModel = (KeyValueSpinnerPojo) parent.getSelectedItem();
                //Utility.showMessage("mvModel ID: " + mvModel.getId() + ",  mvModel Name : " + mvModel.getName(), PropertySearchFormActivity.this);
                PropertySearchFormActivity.this.mv_model_cd = mvModel.getId();

                //
                try {

                    mProgressDialog.show();
                    MasterWebController masterWebController = new MasterWebController();
                    PropertySearchFormActivity.this.requestParamsToServer.m_service = Constant.mMVColorConnect;
                    masterWebController.GetMVColorSpinner(PropertySearchFormActivity.this.requestParamsToServer);
                    recursionMasterCheckMVColor();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }


    /**
     * kindly be careful recursion may be infinite if not handled carefully
     */
    private void recursionMasterCheckMVColor() {


        if (singleton.responseParamsFromServer.STATUS_CODE==null) {
            mProgressDialog.dismiss();
            Utility.showMessage(Constant.SERVER_NO_DATA,PropertySearchFormActivity.this);
            return;
        }

        if (!singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.recursion_code)) {

            mProgressDialog.dismiss();
            if (singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.SUCCESS_200)) {
                setMVColorData();
            }else{
                //Utility.showMessage(Constant.SERVER_NO_DATA,PropertySearchFormActivity.this);
            }


        } else {

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    recursionMasterCheckMVColor();
                }
            }, 2000); // After 2 seconds
        }
    }



    private void setMVColorData() {


        ArrayList<KeyValueSpinnerPojo> mvColorlist = new ArrayList<>();


        mvColorlist.add(new KeyValueSpinnerPojo("", "--Select Color--"));

        for (ResponseParamsFromServer.WSPMVColorRS rs : singleton.responseParamsFromServer.MVColorList) {
            mvColorlist.add(new KeyValueSpinnerPojo(rs.MVColorCd, rs.MVColorName));
        }


        //fill data in spinner
        ArrayAdapter<KeyValueSpinnerPojo> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, mvColorlist);
        sp_color.setAdapter(adapter);
        sp_color.setSelection(0);//Optional to set the selected item.

        sp_color.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                KeyValueSpinnerPojo mvColor = (KeyValueSpinnerPojo) parent.getSelectedItem();
                //Utility.showMessage("mvColor ID: " + mvColor.getId() + ",  mvColor Name : " + mvColor.getName(), PropertySearchFormActivity.this);
                PropertySearchFormActivity.this.mv_color_cd = mvColor.getId();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }


    public void ValidateInput() {

        try {
            if (chkUAP.isChecked() || chkPS.isChecked() || chkFIR.isChecked()) {
                StoreServerInputParams();
            } else {
                Utility.showMessage("Please select any of the search criteria", PropertySearchFormActivity.this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }// end validate


    public void StoreServerInputParams() {


        RequestParamsToServer propertyInput = new RequestParamsToServer();
        propertyInput.m_service = Constant.mPropertySearch;//mandatory
        propertyInput.pagination_counter = ""+singleton.pagination_counter; //mandatory
        propertyInput.P_STATE_CD = Constant.STATE_CD;
        propertyInput.STATE_CD = Constant.STATE_CD;
        propertyInput.DISTRICT_CD = this.Dist_Code;
        propertyInput.PS_CD = this.PS_Code;
        propertyInput.MATCH_CRITERIA_UAP = (chkUAP.isChecked()) ? "true" : "";
        propertyInput.MATCH_CRITERIA_SEIZED = (chkPS.isChecked()) ? "true" : "";
        propertyInput.MATCH_CRITERIA_REGISTERED = (chkFIR.isChecked()) ? "true" : "";
        propertyInput.MV_TYPECD = this.mv_vehicle_cd!=null?this.mv_vehicle_cd:"";
        propertyInput.MV_MAKECD = this.mv_manufacturer_cd!=null?this.mv_manufacturer_cd:"";
        propertyInput.MV_MODELCD = this.mv_model_cd!=null?this.mv_model_cd:"";
        propertyInput.MVCOLOR_CD = this.mv_color_cd!=null?this.mv_color_cd:"";
        propertyInput.REGISTRATION_NO = edt_Reg_Num.getText().toString();
        propertyInput.CHASSIS_NO = edt_Chasis_Num.getText().toString();
        propertyInput.ENGINE_NO = edt_Engine_Num.getText().toString();

        // access to any class in whole project
        singleton.propertySearchFormActivityInput = propertyInput;

        //lets check what is inside parameters
        Gson gson = new Gson(); String json = gson.toJson(singleton.propertySearchFormActivityInput);
        Utility.ConvertObjectToArray(json, this.getClass().getSimpleName());

        // lets go to next
        Utility.doStartActivityWithoutFinish(_acActivity, PropertySearchResultActivity.class, "right");

    }// end validate

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Utility.doAnim(_acActivity, "left");
    }
}//end main class--------------------
