package iif.cctns.in.gov.ncrb.policesearch.webservice;


import android.support.annotation.Keep;


/**
 * Class to represent a category in app.
 *
 * @author
 */
public class RequestParamsToServer {


    // this is important, which service you want to call on server
    @Keep
    public String m_service;

    @Keep
    public String username;
    public String password;

    public String District_Cd;
    public String FxdState_Cd;

    //property search
    public String VType_Cd;
    public String MVMake;
    public String MVColor;


    // person search inputs
    public String P_STATE_CD;
    public String P_DISTRICT_CD;
    public String P_PS_CD;
    public String MATCH_CRITERIA_ARR;
    public String MATCH_CRITERIA_CON;
    public String MATCH_CRITERIA_PO;
    public String MATCH_CRITERIA_CS;
    public String MATCH_CRITERIA_HO;
    public String MATCH_CRITERIA_NOTARR;
    public String P_FName;
    public String P_MName;
    public String P_LName;
    public String P_Alias;
    public String P_GCODE;
    public String P_RELTYPECODE;
    public String P_RelativeName;
    public String P_AgeFrom;
    public String P_AgeTo;
    public String P_HEIGHTFROM;
    public String P_HEIGHTTO;
    public String P_RELGNCODE;
    public String pagination_counter;

    // property input params
    public String STATE_CD;
    public String DISTRICT_CD;
    public String PS_CD;
    public String MATCH_CRITERIA_UAP;
    public String MATCH_CRITERIA_SEIZED;
    public String MATCH_CRITERIA_REGISTERED;
    public String MV_TYPECD;
    public String MV_MAKECD;
    public String MV_MODELCD;
    public String MVCOLOR_CD;
    public String REGISTRATION_NO;
    public String CHASSIS_NO;
    public String ENGINE_NO;

    // FIR Detail
    public String FIR_firregnum;
    public String FIR_district_cd;
    public String FIR_ps_cd;
    public String FIR_year;

    // more detail of person
    public String ACCUSED_SRNO;
    public String FIR_REG_NUM;

    // image of accused
    public String mperson_reg_num;

}// end json post params