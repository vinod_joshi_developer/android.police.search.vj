
package iif.cctns.in.gov.ncrb.policesearch;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.print.PrintManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import iif.cctns.in.gov.ncrb.policesearch.adapter.DetailAdapter;
import iif.cctns.in.gov.ncrb.policesearch.adapter.PropertyDetailAdapter;
import iif.cctns.in.gov.ncrb.policesearch.adapter.PropertySearchResultAdapter;
import iif.cctns.in.gov.ncrb.policesearch.pojo.KeyValueListPojo;
import iif.cctns.in.gov.ncrb.policesearch.pojo.LabelPojo;
import iif.cctns.in.gov.ncrb.policesearch.utils.Singleton;
import iif.cctns.in.gov.ncrb.policesearch.utils.Utility;
import iif.cctns.in.gov.ncrb.policesearch.utils.ViewPrintAdapter;
import iif.cctns.in.gov.ncrb.policesearch.webservice.ResponseParamsFromServer;

public class PropertyDetailActivity extends AppCompatActivity {
    private Activity _acActivity = PropertyDetailActivity.this;
    RecyclerView rvList;
    private TextView tvHeaderName;


    ProgressDialog mProgressDialog;
    Singleton singleton;
    ResponseParamsFromServer.WSPPropertySearchRS wspPropertySearchRS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_detail);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Initalize all widgets
        initView();

    }//end onCreate-------------

    private void initView() {

        singleton = Singleton.getInstance();

        tvHeaderName = findViewById(R.id.tvHeaderName);
        tvHeaderName.setText("Property Details");

        wspPropertySearchRS = singleton.responseParamsFromServer.propertySearchCheck.get(singleton.personSearchRowNo);


        try {
            setPropertyResult();
        } catch (Exception e) {
            e.printStackTrace();
        }


        findViewById(R.id.btn_save_pdf_person).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PropertyDetailActivity.this.printPDF(view);
            }
        });

        findViewById(R.id.btn_fir_person).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFIR();
            }
        });


        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }//end initView------------


    public void showFIR() {

//        String[] year_cum_fir = wspPropertySearchRS.firRegNum.split("/");
//
//        singleton.FIR_firregnum = year_cum_fir[0];// fir number 4 digit only
//        singleton.FIR_district_cd = wspPropertySearchRS.district_cd;
//        singleton.FIR_ps_cd = wspPropertySearchRS.ps_cd;
//
//        String[] year_from_date=  wspPropertySearchRS.reg_dt.split("-");
//        singleton.FIR_year = year_from_date[0];// year of fir


        Utility.doStartActivityWithFinish(PropertyDetailActivity.this, FIROverView.class, "right");

    }



    private void setPropertyResult() throws Exception {


        List<KeyValueListPojo> keyValueListPojos = new ArrayList<KeyValueListPojo>();

        Gson gson = new Gson(); String json = gson.toJson(wspPropertySearchRS);
        keyValueListPojos = Utility.ConvertObjectToArray(json,LabelPojo.PropertyLabel);

        rvList = findViewById(R.id.rvList);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(PropertyDetailActivity.this, LinearLayoutManager.VERTICAL, false);
        rvList.setLayoutManager(mLayoutManager);
        rvList.setItemAnimator(new DefaultItemAnimator());
        //rvList.setAdapter(new PropertySearchResultAdapter(PropertySearchResultActivity.this));
        rvList.setAdapter(new PropertyDetailAdapter(PropertyDetailActivity.this, keyValueListPojos));


    }// end setPropertyResult



    public void printPDF(View view) {

        PrintManager printManager = (PrintManager) getSystemService(PRINT_SERVICE);

        printManager.print("print_any_view_job_name", new ViewPrintAdapter(this, findViewById(R.id.llmid)), null);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Utility.doAnim(_acActivity, "left");
    }
}//end main class--------------------
