package iif.cctns.in.gov.ncrb.policesearch;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

import iif.cctns.in.gov.ncrb.policesearch.utils.Singleton;
import iif.cctns.in.gov.ncrb.policesearch.utils.Utility;

public class HomeActivity extends AppCompatActivity {
    private Activity _acActivity = HomeActivity.this;

    Singleton singleton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        singleton = Singleton.getInstance();

        //Initalize all widgets
        initView();

    }//end onCreate-------------

    private void initView() {

        //reset  pagination counter
        singleton.pagination_counter=0;

        findViewById(R.id.llPersonSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.doStartActivityWithoutFinish(_acActivity, PersonSearchFormActivity.class, "right");
            }
        });

        findViewById(R.id.llPropertySearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.doStartActivityWithoutFinish(_acActivity, PropertySearchFormActivity.class, "right");
            }
        });


    }//end initView------------


}//end main class--------------------
