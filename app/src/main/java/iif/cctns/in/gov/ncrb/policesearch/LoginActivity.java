package iif.cctns.in.gov.ncrb.policesearch;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import iif.cctns.in.gov.ncrb.policesearch.utils.Constant;
import iif.cctns.in.gov.ncrb.policesearch.utils.Singleton;
import iif.cctns.in.gov.ncrb.policesearch.utils.Utility;
import iif.cctns.in.gov.ncrb.policesearch.webservice.ApiCaller;
import iif.cctns.in.gov.ncrb.policesearch.webservice.MasterWebController;
import iif.cctns.in.gov.ncrb.policesearch.webservice.ResponseParamsFromServer;
import iif.cctns.in.gov.ncrb.policesearch.webservice.RequestParamsToServer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    private Activity _acActivity = LoginActivity.this;
    private EditText edUsername, edPassword;

    Singleton singleton;
    ProgressDialog mProgressDialog;
    RequestParamsToServer requestParamsToServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        singleton = Singleton.getInstance();

        //Initalize all widgets
        initView();


    }//end onCreate-------------

    private void initView() {

        requestParamsToServer = new RequestParamsToServer();

        edUsername = findViewById(R.id.edUsername);
        edPassword = findViewById(R.id.edPassword);

    }//end initView------------


    //Press login button
    public void cancelController(View view) {
        finish();
    }//end loginController--------------

    //Press login button
    public void loginController(View view) {
        checkEnteredValuesInLoginPage();
    }//end loginController--------------

    //Validation check for login page
    private void checkEnteredValuesInLoginPage() {


        try {

            if (Utility.getUtilityInstance().getStringFromEditText(edUsername).equalsIgnoreCase(""))
                Utility.showMessage(getString(R.string.please_enter_username), _acActivity);
            else if (Utility.getUtilityInstance().getStringFromEditText(edPassword).equalsIgnoreCase(""))
                Utility.showMessage(getString(R.string.please_enter_password), _acActivity);
            else
                checkCrediential();


        } catch (Exception e) {
            e.printStackTrace();
        }


    }//end checkEnteredValuesInLoginPage-----------

    @Override
    public void onStart() {
        super.onStart();


        mProgressDialog = new ProgressDialog(LoginActivity.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");

    }


    public void checkCrediential() {

        try {

            mProgressDialog.show();
            MasterWebController masterWebController = new MasterWebController();
            requestParamsToServer.m_service = Constant.mLoginVerify;
            requestParamsToServer.username = Utility.getUtilityInstance().getStringFromEditText(edUsername);
            requestParamsToServer.password = Utility.getUtilityInstance().getStringFromEditText(edUsername);
            masterWebController.GetAuthDetailWebService(requestParamsToServer);
            recursionMasterCheck();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /**
     * kindly be careful recursion may be infinite if not handled carefully
     */
    private void recursionMasterCheck() {


        try {

            if (singleton.responseParamsFromServer == null) {
                mProgressDialog.dismiss();
                Utility.showMessage("Server Error", LoginActivity.this);
                return;
            }

            Utility.printv("singleton.responseParamsFromServer.STATUS_CODE" + singleton.responseParamsFromServer.STATUS_CODE);

            if (!singleton.responseParamsFromServer.STATUS_CODE.equals(Constant.recursion_code)) {

                mProgressDialog.dismiss();

                loginActionResult();

            } else {

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        recursionMasterCheck();
                    }
                }, 2000); // After 5 seconds
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void loginActionResult() {


        if (singleton.responseParamsFromServer == null ) {
            Utility.showMessage("Incorrect detail. Try again", LoginActivity.this);
            return;
        }

        // todo temp allow login to all = TESTING

        Utility.doStartActivityWithFinish(LoginActivity.this, HomeActivity.class, "right");

//        if (singleton.responseParamsFromServer.STATUS_CODE.equals("200")) {
//
//            // keep the username for next use
//            this.singleton.username = Utility.getUtilityInstance().getStringFromEditText(edUsername);
//
//            Utility.doStartActivityWithFinish(LoginActivity.this, HomeActivity.class, "right");
//
//        } else {
//
//            Utility.showMessage("Incorrect detail. Try again", LoginActivity.this);
//        }

    }// end login action


}//end main class--------------------
