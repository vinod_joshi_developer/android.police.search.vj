package iif.cctns.in.gov.ncrb.policesearch;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import iif.cctns.in.gov.ncrb.policesearch.adapter.DetailAdapter;
import iif.cctns.in.gov.ncrb.policesearch.adapter.PersonSearchResultAdapter;
import iif.cctns.in.gov.ncrb.policesearch.utils.Utility;

public class PersonDetailActivity extends AppCompatActivity {
    private Activity _acActivity = PersonDetailActivity.this;
    RecyclerView rvList;
    private TextView tvHeaderName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_detail);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Initalize all widgets
        initView();

    }//end onCreate-------------

    private void initView() {

        tvHeaderName = findViewById(R.id.tvHeaderName);
        tvHeaderName.setText("Person Details");

        rvList = findViewById(R.id.rvList);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(PersonDetailActivity.this, LinearLayoutManager.VERTICAL, false);
        rvList.setLayoutManager(mLayoutManager);
        rvList.setItemAnimator(new DefaultItemAnimator());
        rvList.setAdapter(new DetailAdapter(PersonDetailActivity.this));

        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }//end initView------------


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Utility.doAnim(_acActivity, "left");
    }
}//end main class--------------------
