package iif.cctns.in.gov.ncrb.policesearch;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import iif.cctns.in.gov.ncrb.policesearch.utils.Utility;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Move Login Page after 3 sec
        moveOnLoginPage();

    }//end onCreate--------------

    private void moveOnLoginPage() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Utility.doStartActivityWithFinish(SplashActivity.this, LoginActivity.class, "right");

            }
        }, 3000);

    }//end moveOnLoginPage-----------

}//end main class--------------------
