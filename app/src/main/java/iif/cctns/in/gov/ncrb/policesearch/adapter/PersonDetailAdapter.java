package iif.cctns.in.gov.ncrb.policesearch.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import iif.cctns.in.gov.ncrb.policesearch.R;
import iif.cctns.in.gov.ncrb.policesearch.pojo.KeyValueListPojo;
import iif.cctns.in.gov.ncrb.policesearch.webservice.ResponseParamsFromServer;


/**
 * Created by Vishnu
 */
public class PersonDetailAdapter extends RecyclerView.Adapter<PersonDetailAdapter.MyViewHolder> {

    Activity mContext;
    //private List<ResponseParamsFromServer.WSPPersonSearchRS> personSearchCheck;

    private List<KeyValueListPojo> keyValueListPojos ;

    public PersonDetailAdapter(Activity activity,List<KeyValueListPojo> keyValueListPojos) {
        this.mContext = activity;
        this.keyValueListPojos = keyValueListPojos;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_name, txt_value;

        LinearLayout llMain;

        public MyViewHolder(View view) {
            super(view);
            llMain = view.findViewById(R.id.llMain);
            txt_name = view.findViewById(R.id.txt_name);
            txt_value = view.findViewById(R.id.txt_value);

        }

    }// end MyViewHolder class...........


    // add single item view
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_two_section, parent, false);

        return new MyViewHolder(itemView);
    }

    // Used to set Tire
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if (position % 2 == 0)
            holder.llMain.setBackgroundColor(mContext.getResources().getColor(R.color.bg_color));
        else
            holder.llMain.setBackgroundColor(mContext.getResources().getColor(R.color.white));

        KeyValueListPojo keyValueListPojo = keyValueListPojos.get(position);
        holder.txt_name.setText(keyValueListPojo.keyName);
        holder.txt_value.setText(keyValueListPojo.valueOfKey);

    }


    @Override
    public int getItemCount() {
        return this.keyValueListPojos.size();
    }

}// end main class............................