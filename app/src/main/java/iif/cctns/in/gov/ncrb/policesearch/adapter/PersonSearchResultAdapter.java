package iif.cctns.in.gov.ncrb.policesearch.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import iif.cctns.in.gov.ncrb.policesearch.PersonDetailActivity;
import iif.cctns.in.gov.ncrb.policesearch.PersonDetailAtvt;
import iif.cctns.in.gov.ncrb.policesearch.R;
import iif.cctns.in.gov.ncrb.policesearch.utils.Singleton;
import iif.cctns.in.gov.ncrb.policesearch.utils.Utility;
import iif.cctns.in.gov.ncrb.policesearch.webservice.ResponseParamsFromServer;


/**
 * Created by
 */
public class PersonSearchResultAdapter extends RecyclerView.Adapter<PersonSearchResultAdapter.MyViewHolder> {

    Activity mContext;
    Singleton singleton;
    private List<ResponseParamsFromServer.WSPPersonSearchRS> personSearchCheck;


    public PersonSearchResultAdapter(Activity activity,List<ResponseParamsFromServer.WSPPersonSearchRS> personSearchCheck) {
        this.mContext = activity;
        this.personSearchCheck = personSearchCheck;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_person_name, txt_person_alias, txt_person_relation_type, txt_person_relative_name;

        LinearLayout llRow;

        public MyViewHolder(View view) {
            super(view);
            llRow = view.findViewById(R.id.llRow);
            txt_person_name = view.findViewById(R.id.txt_person_name);
            txt_person_alias = view.findViewById(R.id.txt_person_alias);
            txt_person_relation_type = view.findViewById(R.id.txt_person_relation_type);
            txt_person_relative_name = view.findViewById(R.id.txt_person_relative_name);
        }

    }// end MyViewHolder class...........


    // add single item view
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_search_person_result, parent, false);

        return new MyViewHolder(itemView);
    }

    // Used to set Tire
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if (position % 2 == 0)
            holder.llRow.setBackgroundColor(mContext.getResources().getColor(R.color.bg_color));
        else
            holder.llRow.setBackgroundColor(mContext.getResources().getColor(R.color.white));

        ResponseParamsFromServer.WSPPersonSearchRS wspPersonSearchRS = personSearchCheck.get(position);
        holder.txt_person_name.setText(wspPersonSearchRS.FULL_NAME);
        holder.txt_person_alias.setText(wspPersonSearchRS.alias1);
        holder.txt_person_relation_type.setText(wspPersonSearchRS.relation_type);
        holder.txt_person_relative_name.setText(wspPersonSearchRS.RELATIVE_NAME);


        holder.llRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                singleton = Singleton.getInstance();
                singleton.personSearchRowNo = position;
                Utility.doStartActivityWithoutFinish(mContext, PersonDetailAtvt.class, "right");
            }
        });

    }


    @Override
    public int getItemCount() {
        return this.personSearchCheck.size();
    }

}// end main class............................