package iif.cctns.in.gov.ncrb.policesearch.pojo;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import iif.cctns.in.gov.ncrb.policesearch.utils.Utility;
import iif.cctns.in.gov.ncrb.policesearch.webservice.ResponseParamsFromServer;

/**
 * Vishnu
 */

public class LabelPojo {


    // label name must be given as method name
    public static String PersonLabel = "PersonLabel";

    public static Map PersonLabel() {

        Map<String, String> map = new HashMap<>();
        map.put("accused_srno", "Accused Serial No");
        map.put("uid_num", "UID No");
        map.put("REG_DT", "Registration Date");
        //map.put("REGISTRATION_NUM", "Full FIR");
        map.put("REG_NUM", "FIR");
        map.put("DOB", "Date Of Birth");
        map.put("ARREST_SURR_DT", "Arrest Surrend Date");
        map.put("MatchingCriteria", "Matching Criteria");
        //map.put("SectionCd", "Section Code");
        map.put("Section", "Act/Section");
        map.put("all_accused", "All Accused");
        map.put("FULL_NAME", "Full Name");
        map.put("STATE_ENG", "State");
        map.put("district", "District");
        map.put("ps", "Police Station");
        map.put("relation_type", "Relation Type");
        map.put("gender", "Gender");
        map.put("religion", "Religion");
        map.put("RELATIVE_NAME", "Relative Name");
        map.put("age", "Age");
        //map.put("PERSON_CODE", "Person Code");
        //map.put("lang_cd", "Lang Code");
        //map.put("ImageID", "Image ID");
        //map.put("UploadedFile", "Upload File No");
        map.put("PERMANENT", "Address Permanent");
        map.put("PRESENT", "Address Present");
        map.put("alias1", "Alias");
        map.put("height_to_cm", "Height To CM");
        map.put("height_from_cm", "Height From CM");
        //map.put("STATE", "State");
        //map.put("accusedsrNo", "Accused Sr No");
        //map.put("state_cd", "State Code");
        //map.put("district_cd", "District Code");
        //map.put("ps_cd", "Police Station Code");

        return map;

    }


    // label name must be given as method name
    public static String MoreDetailPersonLabel = "MoreDetailPersonLabel";

    public static Map MoreDetailPersonLabel() {

        Map<String, String> map = new HashMap<>();
        map.put("arrest_surrend", "");
        map.put("ArrestedResult", "Arrested Result");
        map.put("ARREST_SURRENDER_DT", "Date");
        map.put("ARR_SURR_STATE_CD", "State Code");
        map.put("ARR_SURR_DISTRICT_CD", "District Code");
        map.put("occupation", "Occupation");
        map.put("ARR_SURR_PS_CD", "Police Station Code");
        map.put("SectionCode", "Section Code");
        map.put("section", "Section");
        map.put("SURREND_IN_COURT_CD", "Surrender in court code");
        map.put("SURREND_MAGISTRATE", "Surrender Magistrate");
        map.put("STATE_ENG", "State");
        map.put("DISTRICT", "District");
        map.put("PS", "Police Station");
        map.put("ARREST_ACTION", "Arrest Action");
        map.put("SURRENDERED_COURT", "Surrendered Court");

        return map;
    }

    // label name must be given as method name
    public static String PersonMoreDetailsConvictLabel = "PersonMoreDetailsConvictLabel";

    public static Map PersonMoreDetailsConvictLabel() {

        Map<String, String> map = new HashMap<>();
        map.put("PUNISHMENT_TYPE_CD", "Punishment Type Code");
        map.put("PUNISHMENT_TYPE", "Punishment Type");
        map.put("judgement_date", "Judgement Date");
        map.put("PUNISH_YRS", "Punish Years");
        map.put("PUNISH_MNTH", "Punish Months");
        map.put("PUNISH_DAYS", "Punish Days");
        map.put("IsConvicted", "Is Convicted");

        return map;
    }


    // label name must be given as method name
    public static String PersonMoreDetailsPHOffenderLabel = "PersonMoreDetailsPHOffenderLabel";

    public static Map PersonMoreDetailsPHOffenderLabel() {

        Map<String, String> map = new HashMap<>();
        map.put("PROCL_OFFENDER_COURT_TYPE", "Offender Court Type");
        map.put("PROCL_OFFENDER_COURT_NAME", "Offender Court Name");
        map.put("PROCL_OFFENDER_COURT_LOCATION", "Offender Court Location");
        map.put("PROCL_OFFENDER_ORDER_NUM", "Offender Order No");
        map.put("PROCL_OFFENDER_ORDER_DT", "Offender Order Date");
        map.put("COURT_TYPE", "Court Type");
        map.put("is_proclaimed_offender", "Is Proclaimed Offender");
        map.put("HabitualOffender", "Habitual Offender");

        return map;
    }


    // label name must be given as method name
    public static String PropertyLabel = "PropertyLabel";
    public static Map PropertyLabel() {

        Map<String, String> map = new HashMap<>();

        map.put("reg_dt", "Registration Date");
        map.put("firRegNum", "FIR No");
        //map.put("fullfirnumber", "Complete FIR No");
        map.put("GD_No", "GD No");
        map.put("firstName", "First Name");
        map.put("STATE", "State");
        map.put("district", "District");
        map.put("ps", "Police Station");
        map.put("propertyNature", "Property Nature");
        map.put("registrationNo", "Registration No");
        map.put("mvType", "Vehicle Type");
        map.put("mvModel", "Vehicle Model");
        map.put("mvMake", "Vehicle Manufacturer");
        map.put("mvColor", "Vehicle Color");
        map.put("chasisNo", "Chasis No");
        map.put("engineNo", "Engine No");
        map.put("Matching_Criteria", "Matching Criteria");
        //map.put("state_cd", "State Code");
        //map.put("district_cd", "District Code");
        //map.put("ps_cd", "Police Station Code");

        return map;


    }


}// end ReligionPojo